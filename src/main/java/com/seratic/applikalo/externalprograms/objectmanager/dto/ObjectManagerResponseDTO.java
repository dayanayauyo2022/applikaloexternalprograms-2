package com.seratic.applikalo.externalprograms.objectmanager.dto;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.ArrayList;
import java.util.List;

public class ObjectManagerResponseDTO {

    private List<JsonNode> data = new ArrayList<>(0);
    private boolean success;
    private int total;

    public ObjectManagerResponseDTO() {
    }

    public List<JsonNode> getData() {
        return data;
    }

    public void setData(List<JsonNode> data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
