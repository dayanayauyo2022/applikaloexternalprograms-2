//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.11.23 a las 12:16:31 PM COT 
//
package com.seratic.applikalo.externalprograms.modules.solgas.soap.gen;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonTypeName;
import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para medidor complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType name="medidor">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="caudal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modelo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="marca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="activomedidor" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="presionfabrica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="edificio" type="{http://www.seratic.com/applikalo/externalprograms/modules/solgas/soap/gen}edificio" minOccurs="0"/>
 *         &lt;element name="ultimalectura" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ultimoconsumo" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="consumo3meses" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="consumopromedio" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="interior" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cliente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "medidor", propOrder = {
    "id",
    "codigo",
    "caudal",
    "modelo",
    "marca",
    "activomedidor",
    "presionfabrica",
    "edificio",
    "ultimalectura",
    "ultimoconsumo",
    "consumo3Meses",
    "consumopromedio",
    "interior",
    "cliente"
})
@JsonTypeName(value = "medidor")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Medidor {

    @JsonInclude(JsonInclude.Include.NON_NULL)
    protected Integer id;
    protected String codigo;
    protected String caudal;
    protected String modelo;
    protected String marca;
    protected Boolean activomedidor;
    protected String presionfabrica;
    @JsonIgnoreProperties({"envionotificacion", "correonotificacion", "activoedificio", "tipo", "distrito", "provincia", "departamento", "ubicacion", "direccion", "nombre"})
    protected Edificio edificio;
    protected BigDecimal ultimalectura;
    protected BigDecimal ultimoconsumo;
    @XmlElement(name = "consumo3meses")
    protected Boolean consumo3Meses;
    protected BigDecimal consumopromedio;
    protected String interior;
    protected String cliente;

    /**
     * Obtiene el valor de la propiedad id.
     *
     * @return possible object is {@link Integer }
     *
     */
    public Integer getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     *
     * @param value allowed object is {@link Integer }
     *
     */
    public void setId(Integer value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad codigo.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Define el valor de la propiedad codigo.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setCodigo(String value) {
        this.codigo = value;
    }

    /**
     * Obtiene el valor de la propiedad caudal.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCaudal() {
        return caudal;
    }

    /**
     * Define el valor de la propiedad caudal.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setCaudal(String value) {
        this.caudal = value;
    }

    /**
     * Obtiene el valor de la propiedad modelo.
     *
     * @return possible object is {@link String }
     *
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * Define el valor de la propiedad modelo.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setModelo(String value) {
        this.modelo = value;
    }

    /**
     * Obtiene el valor de la propiedad marca.
     *
     * @return possible object is {@link String }
     *
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Define el valor de la propiedad marca.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setMarca(String value) {
        this.marca = value;
    }

    /**
     * Obtiene el valor de la propiedad activomedidor.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean isActivomedidor() {
        return activomedidor;
    }

    /**
     * Define el valor de la propiedad activomedidor.
     *
     * @param value allowed object is {@link Boolean }
     *
     */
    public void setActivomedidor(Boolean value) {
        this.activomedidor = value;
    }

    /**
     * Obtiene el valor de la propiedad presionfabrica.
     *
     * @return possible object is {@link String }
     *
     */
    public String getPresionfabrica() {
        return presionfabrica;
    }

    /**
     * Define el valor de la propiedad presionfabrica.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setPresionfabrica(String value) {
        this.presionfabrica = value;
    }

    /**
     * Obtiene el valor de la propiedad edificio.
     *
     * @return possible object is {@link Edificio }
     *
     */
    public Edificio getEdificio() {
        return edificio;
    }

    /**
     * Define el valor de la propiedad edificio.
     *
     * @param value allowed object is {@link Edificio }
     *
     */
    public void setEdificio(Edificio value) {
        this.edificio = value;
    }

    /**
     * Obtiene el valor de la propiedad ultimalectura.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getUltimalectura() {
        return ultimalectura;
    }

    /**
     * Define el valor de la propiedad ultimalectura.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setUltimalectura(BigDecimal value) {
        this.ultimalectura = value;
    }

    /**
     * Obtiene el valor de la propiedad ultimoconsumo.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getUltimoconsumo() {
        return ultimoconsumo;
    }

    /**
     * Define el valor de la propiedad ultimoconsumo.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setUltimoconsumo(BigDecimal value) {
        this.ultimoconsumo = value;
    }

    /**
     * Obtiene el valor de la propiedad consumo3Meses.
     *
     * @return possible object is {@link Boolean }
     *
     */
    public Boolean isConsumo3Meses() {
        return consumo3Meses;
    }

    /**
     * Define el valor de la propiedad consumo3Meses.
     *
     * @param value allowed object is {@link Boolean }
     *
     */
    public void setConsumo3Meses(Boolean value) {
        this.consumo3Meses = value;
    }

    /**
     * Obtiene el valor de la propiedad consumopromedio.
     *
     * @return possible object is {@link BigDecimal }
     *
     */
    public BigDecimal getConsumopromedio() {
        return consumopromedio;
    }

    /**
     * Define el valor de la propiedad consumopromedio.
     *
     * @param value allowed object is {@link BigDecimal }
     *
     */
    public void setConsumopromedio(BigDecimal value) {
        this.consumopromedio = value;
    }

    /**
     * Obtiene el valor de la propiedad interior.
     *
     * @return possible object is {@link String }
     *
     */
    public String getInterior() {
        return interior;
    }

    /**
     * Define el valor de la propiedad interior.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setInterior(String value) {
        this.interior = value;
    }

    /**
     * Obtiene el valor de la propiedad cliente.
     *
     * @return possible object is {@link String }
     *
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * Define el valor de la propiedad cliente.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setCliente(String value) {
        this.cliente = value;
    }

}
