package com.seratic.applikalo.externalprograms.modules.solgas.utils;

public class ConstantesStatic {

    public static final String KEYFECHACREACION = "fecha_creacion";
    public static final String KEYOBJETO = "objeto";
    public static final String KEYLECTURAMEDIDOR = "lectura_medidor";
    public static final String KEYSTATUSCODE = "statusCode";
    public static final String KEYMEDIDOR = "Medidor";
    public static final String KEYMEDIDOROBJ = "medidor";
    public static final String MSJNOCONNECT_API = "No\"M fue posible obtener mediciones de meses anteriores";
    public static final String KEYLECTURAACTUAL = "lectura_actual";

}
