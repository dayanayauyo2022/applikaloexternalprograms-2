package com.seratic.applikalo.externalprograms.modules.amauta.utils;

import java.util.List;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Esta clase es una utilidad que tiene metodos para construir 
 * bodys de forma dinamica para ser usados en el llamado del api de objetos
 *
 * @author Mario Jurado
 * @version 1.0
 * @since 2021-03-16
 */

public final class CrearBodyUtils {

    /**
     * Metodo estatico para retornar un filtro de body 
     * 
     * @param campo string del campo por el cual se hace el filtro
     * @param condicional string del condicional para el filtro valores posibles 
     * IN, BETWEEN, =, etc
     * @param conjuncion string para la conjuncion usada entre varios filtros valores posibles AND, OR
     * @param parametro string correspondiente al valor que por el cual se efectua el parametro 
     * @return JsonNode, con la estructura necesaria para los filtros aplicados en las peticiones del api.
   */
    public static JsonNode obtenerFiltro(String campo, String condicional, String conjuncion, String objeto, String parametro){
        
        ObjectMapper mapper = new ObjectMapper();
            
        JsonNode filtro = mapper.createObjectNode();
        ((ObjectNode) filtro).put("campo", campo);
        ((ObjectNode) filtro).put("condicional", condicional);
        ((ObjectNode) filtro).put("conjuncion", conjuncion);
        ((ObjectNode) filtro).put("objeto", objeto);
        ((ObjectNode) filtro).put("parametro", parametro);
        
        return filtro;
    }

    /**
     * Metodo estatico para retornar un campo del body 
     * 
     * @param nombreCampo string correspondiente nombre del campo 
     * @return JsonNode, con la estructura necesaria para los campos aplicados en las peticiones del api.
   */
    public static JsonNode obtenerCampo(String nombreCampo){
        
        ObjectMapper mapper = new ObjectMapper();

        JsonNode campo = mapper.createObjectNode();
        ((ObjectNode) campo).put("campo", nombreCampo);

        return campo;
    }

    /**
     * Metodo estatico para retornar un condicional IN para filtros 
     * 
     * @param elementos lista de strings corresponientes a los valores que van dentro del filtro IN 
     * @return string con la cadena del IN.
   */
    public static String obtnerCondicionIn(List<String> elementos){
        String cadenaIn = "";
        for(int i = 0; i < elementos.size(); i++) {
            cadenaIn += "'" + elementos.get(i) + "'";
            if(i != (elementos.size()-1) ){
                cadenaIn += " ,";
            }
        }
        return cadenaIn;
    }

    /**
     * Metodo estatico para retornar un condicional BETWEEn para filtros 
     * 
     * @param elementos lista de strings corresponientes a los valores que van dentro del filtro BETWEEN 
     * @return string con la cadena del BETWEEN.
   */
    public static String obtnerCondicionBetween(List<String> elementos){
        String cadenaBetween = "";
        for(int i = 0; i < elementos.size(); i++) {
            cadenaBetween += "'" + elementos.get(i) + "'";
            if(i != (elementos.size()-1) ){
                cadenaBetween += " AND ";
            }
        }
        return cadenaBetween;
    }
}
