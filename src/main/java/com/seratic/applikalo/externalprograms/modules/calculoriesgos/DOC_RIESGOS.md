# /riesgos/calculoRiesgo #

Servicio POST para la ejecución del programa de cálculo de riesgos. A partir de un formulario se 
hace un análisis con unos parámetros establecidos y se establece un nivel de riesgo, es un maestro
que se relaciona con el objeto que llega en el body de la petición (objeto base).
El proceso inicia con la búsqueda del ultimo nodo de chequeo_sintomas en la tabla wf, luego se obtiene el nivel de
riesgo que es una relación con un maestro y se actualiza el objeto base (chequeo_sintomas) con el nuevo nivel de 
riego. Para obtener este nivel se comparan los campos del objeto chequeo_sintomas con unos parámetros definidos
y se determina un nivel bajo, medio o alto dependiendo de sus valores.

### Ejemplo ###

* URL: http://ema2progextbeta.latinapps.co/riesgos/calculoRiesgo
* Headers: ``` token : eyJhbGciOiJIUzI1NiJ9.eyJpZFB1YmxpY28iOiJkODNiM2FmYjJiNTc4Nzk1OGJjMTQzMGQ3ZjI3NGU1NmFmMWNhYWNlNDRiOTg5NjU2MzQ1YTc0OGRhNjNhMWEzIiwidmVyc2lvbkFwbGljYWNpb24iOjU4MywicHJlZmlqbyI6ImRlc2EiLCJ1cmxBcGlSZWFkIjoiaHR0cHM6Ly9lbWEycmVhZGVyZGVzYS5sYXRpbmFwcHMuY28vYXBpcmVhZC8iLCJleHAiOjE1OTE5MzIwNTIsInVybEFwaVdyaXRlIjoiaHR0cDovL2xvY2FsaG9zdDo4MDg4LyJ9.osieSpRUtXkc2Fkteb2HKGXbcKmaXiLU44ei5vXPSxY```
* Body:
    ```
        {
          "objeto": {
              "id":3,
              "key": "69fbd59363e40c5eddb3de3b932276c4f9d6e5802ef832e5880de5afccf08594"
          },
          "nombreObjeto": "mr_registro_de_tarea"          
        }
    ```