/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seratic.applikalo.externalprograms.modules.prosegur.repository;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.seratic.applikalo.externalprograms.modules.prosegur.dto.SubscriptionDetailDataDTO;
import com.seratic.applikalo.externalprograms.modules.prosegur.dto.SuscriptionDTO;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.Objects;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author ivang
 */
@Repository
public class KitePlatformRepository {

    private final Logger logger = LoggerFactory.getLogger(KitePlatformRepository.class);
    @Value("${prosegur.kitePlatform.inventoryURL}")
    private String suscriptionURL;
    @Autowired
    ObjectMapper objectMapper;
    private RestTemplate restTemplate;

    public KitePlatformRepository(@Value("${prosegur.kitePlatform.sslContextType}") String sslContextType,
            @Value("${prosegur.kitePlatform.keyStoreType}") String keyStoreType,
            @Value("${prosegur.kitePlatform.trustStoreType}") String trustStoreType,
            @Value("${prosegur.kitePlatform.keyStorePath}") String keyStorePath,
            @Value("${prosegur.kitePlatform.trustStorePath}") String trustStorePath,
            @Value("${prosegur.kitePlatform.keyStorePassword}") String keyStorePassword,
            @Value("${prosegur.kitePlatform.trustStorePassword}") String trustStorePassword) {
        try {
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            KeyStore trustStore = KeyStore.getInstance(trustStoreType);
            try ( InputStream keyStoreInputStream = getClass().getResourceAsStream(keyStorePath);  InputStream trustStoreInputStream = getClass().getResourceAsStream(trustStorePath)) {
                Objects.requireNonNull(keyStoreInputStream);
                Objects.requireNonNull(trustStoreInputStream);
                keyStore.load(keyStoreInputStream, keyStorePassword.toCharArray());
                trustStore.load(trustStoreInputStream, trustStorePassword.toCharArray());
            }
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            keyManagerFactory.init(keyStore, keyStorePassword.toCharArray());
            KeyManager[] keyManagers = keyManagerFactory.getKeyManagers();

            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(trustStore);
            TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();

            SSLContext sslContext = SSLContext.getInstance(sslContextType);
            sslContext.init(keyManagers, trustManagers, new SecureRandom());
            HttpClient httpClient = HttpClients.custom().setSSLContext(sslContext).build();

            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
            requestFactory.setHttpClient(httpClient);
            restTemplate = new RestTemplate(requestFactory);
        } catch (Exception e) {
            logger.error("KitePlatformRepository fallo", e);
        }
    }

    public boolean modifySuscription(SuscriptionDTO suscriptionDTO) {
        boolean retorno = false;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            String resourceUrl = suscriptionURL + "/icc:" + suscriptionDTO.getIcc();
            HttpEntity<SuscriptionDTO> httpEntity = new HttpEntity<>(suscriptionDTO, headers);
            ResponseEntity<Void> responseEntity = restTemplate.exchange(resourceUrl, HttpMethod.PUT, httpEntity, Void.class);
            if ((responseEntity.getStatusCode() == HttpStatus.OK) || (responseEntity.getStatusCode() == HttpStatus.NO_CONTENT)) {
                retorno = true;
            }
        } catch (Exception e) {
            logger.error("modifySuscription fallo", e);
        }
        return retorno;
    }

    public SuscriptionDTO getSuscription(String icc) {
        SuscriptionDTO retorno = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            String resourceUrl = suscriptionURL + "/icc:" + icc;
            HttpEntity<SuscriptionDTO> httpEntity = new HttpEntity<>(null, headers);
            ResponseEntity<String> responseEntity = restTemplate.exchange(resourceUrl, HttpMethod.GET, httpEntity, String.class);
            if (responseEntity.getStatusCode() == HttpStatus.OK) {
                SubscriptionDetailDataDTO subscriptionDetailDataDTO = objectMapper.readValue(responseEntity.getBody(), SubscriptionDetailDataDTO.class);
                if (subscriptionDetailDataDTO != null) {
                    retorno = subscriptionDetailDataDTO.getSuscriptionDTO();
                    retorno.setIcc(icc);
                }
            }
        } catch (Exception e) {
            logger.error("getSuscription fallo", e);
        }
        return retorno;
    }
}
