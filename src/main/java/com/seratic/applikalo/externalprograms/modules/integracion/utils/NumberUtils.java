package com.seratic.applikalo.externalprograms.modules.integracion.utils;

public class NumberUtils {

    /*Método para redondear decimales*/
    public static Double formatearDecimales(Double numero, Integer numeroDecimales) {
        return Math.round(numero * Math.pow(10, numeroDecimales)) / Math.pow(10, numeroDecimales);
    }
}
