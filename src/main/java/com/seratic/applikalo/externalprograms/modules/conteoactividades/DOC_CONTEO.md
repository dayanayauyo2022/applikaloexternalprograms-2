## /actividades/contarActividadesSinRagonEstados

Servicio POST para la ejecución del programa de contar actividades y oportunidades. Primero se verifica si el nodo
que disparó este programa Externo es un registroactividad o una oportunidad, de lo contrario no hace nada. Luego, si
se trata de un nodo registroactividad se procede a verificar el tipoactividad para determinar si se debe contar o no
a partir de registroactividad.tipoactividad.contar y si este campo esta en true, se debe obtener el nombre del 
contador a partir de registroactividad.tipoactividad.nombre_contador, luego se concatena este nombre con la palabra
"_dia" y "_mes", es decir se obtiene dos campos para realizar el conteo de la actividad, por lo tanto se
tiene los campos nombreContador_dia y nombreContador_mes, los cuales se verifican si ya exiten en el objeto base y se
acumulan, luego se actualiza el objeto base.
Si el nodo que disparó este programa Externo es una oportunidad, se generan los campos oportunidades_dia y 
oportunidades_mes, se verifica que existan para ser acumulados y luego actualizar el objeto base.

### Ejemplo ###

* URL: http://ema2progextbeta.latinapps.co/actividades/conteoActividadesSinRangoEstados
* Headers: ``` token : eyJhbGciOiJIUzI1NiJ9.eyJpZFB1YmxpY28iOiJkODNiM2FmYjJiNTc4Nzk1OGJjMTQzMGQ3ZjI3NGU1NmFmMWNhYWNlNDRiOTg5NjU2MzQ1YTc0OGRhNjNhMWEzIiwidmVyc2lvbkFwbGljYWNpb24iOjU4MywicHJlZmlqbyI6ImRlc2EiLCJ1cmxBcGlSZWFkIjoiaHR0cHM6Ly9lbWEycmVhZGVyZGVzYS5sYXRpbmFwcHMuY28vYXBpcmVhZC8iLCJleHAiOjE1OTE5MzIwNTIsInVybEFwaVdyaXRlIjoiaHR0cDovL2xvY2FsaG9zdDo4MDg4LyJ9.osieSpRUtXkc2Fkteb2HKGXbcKmaXiLU44ei5vXPSxY```
* Body:
    ```
        {
          "objeto": {
              "id":3,
              "key": "69fbd59363e40c5eddb3de3b932276c4f9d6e5802ef832e5880de5afccf08594"
          },
          "nombreObjeto": "mr_registro_de_tarea"          
        }
    ```

## /actividades/reiniciarContadores(CRON)

Es una tarea automática que se ejecuta cada día a las 12:30 am. El proceso consiste en consultar la colección 
"objeto_conteo" de la bd mongo para obtener todos los objetos de todos los ambientes que 
tiene contadores, se los recorre y para los contadores de dias se establecen en cero, para los 
contadores de meses se verifica si ya es un nuevo mes se establece en cero de lo contrario se le suma 
el valor del contador del día.

**Nota**: la tabla objeto_conteo de la bd mongo se va actualizando en la ejecución del programa externo
de contarActividadesSinRagonEstados.

Para ejecutar este proceso en cualquier momento se dispone de un servicio GET:

* URL: http://ema2progextbeta.latinapps.co/actividades/reiniciarContadores

