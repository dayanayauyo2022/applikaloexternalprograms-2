package com.seratic.applikalo.externalprograms.modules.security;

import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.slf4j.LoggerFactory;

public class TokenManager {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(TokenManager.class);
    private static final String SECRET = "S3R4TIC_4PIR34D";

    public static DataToken getDataSourceInfo(String jwt) {

        DataToken dataToken = new DataToken();
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(SECRET.getBytes("UTF-8"))
                    .parseClaimsJws(jwt).getBody();
            dataToken.setPrefijo(claims.get("prefijo").toString());
            dataToken.setVersionAplicacion((Integer) claims.get("versionAplicacion"));
            dataToken.setIdPublico(claims.get("idPublico").toString());
            if (claims.get("urlApiWrite") != null) {
                dataToken.setUrlApiWrite(claims.get("urlApiWrite").toString());
            }
            if (claims.get("urlApiRead") != null) {
                dataToken.setUrlApiRead(claims.get("urlApiRead").toString());
            }
        } catch (Exception ex) {
            dataToken = null;
            logger.info("!! ERROR - No se logró descifrar Token - excepción " + ex.getMessage());
        }
        return dataToken;
    }

}
