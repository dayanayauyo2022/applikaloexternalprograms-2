package com.seratic.applikalo.externalprograms.modules.activosmineros.utils;

public class ConstantesLlave {

    public static final String KEYAUTHORIZATION = "Authorization";
    public static final String KEYUSERAGENT = "user-agent";
    public static final String KEYATRIBUTOS = "atributos";
    public static final String KEYFECHAFUERADEPLAZO = "fechaplazoproyecto";
    public static final String KEYFECHASUPERVISION = "fecha_supervicion";
    public static final String KEYNOMBRE = "nombrecorto";
    public static final String KEYESTADOPROYECTO = "estado_proyecto";

    // ID TIPO COMPONENTES
    public static final Integer TIPO_COMPONENTE_TABLA = 3032;

    private ConstantesLlave() {
    }
}
