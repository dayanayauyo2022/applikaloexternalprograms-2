package com.seratic.applikalo.externalprograms.modules.integracion.entities.errors;

/* Clase utilizada para retornar errores y excepciones presentados en la ejecusion de los servicios con el objetivo de
 * indicar al cliente la causa y donde se pudo generarse el error */
public class MyError {

    String code;
    String answer;
    String message;

    public MyError() {
    }

    public MyError(String code, String answer, String message) {
        this.code = code;
        this.answer = answer;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
