package com.seratic.applikalo.externalprograms.modules.amauta;

import com.seratic.applikalo.externalprograms.modules.amauta.VOs.AsignarLeccionesResponse;
import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST})
@RequestMapping(value = "/amauta")
public class AmautaController {

    private static final Logger logger = LoggerFactory.getLogger(AmautaController.class);

    @Autowired
    AmautaService amautaService;

    @RequestMapping(method = POST, value = "/asignarLecciones", produces="application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> asignarLecciones(@RequestBody String requestBody,
                                              @RequestAttribute("prefijo") String prefijo,
                                              @RequestAttribute("versionAplicacion") Integer versionAplicacion,
                                              @RequestAttribute("idPublico") String idPublico,
                                              @RequestAttribute("urlApiWrite") String urlApiWrite,
                                              @RequestAttribute("urlApiRead") String urlApiRead) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            AsignarLeccionesResponse out = amautaService.asignarLecciones(requestBody, prefijo, versionAplicacion,
                    idPublico, urlApiWrite, urlApiRead);
            if (out.getEstudianteCurso() != null && out.getEstudianteCurso().isEstado()) {
                return new ResponseEntity<>(out, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(out, HttpStatus.BAD_REQUEST);
            }
        } catch (MyErrorListException myEx) {
            if (myEx.getErrors() != null && myEx.getErrors().size() > 0) {
                logger.info("error ws asignarLecciones: " + myEx.getErrors().get(0).getMessage());
            }
            return new ResponseEntity<>(new JSONSerializer()
                    .exclude("*.class")
                    .exclude("cause")
                    .exclude("localizedMessage")
                    .exclude("stackTraceDepth")
                    .serialize(myEx.getErrors()), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            logger.info("error ws asignarLecciones: ex: " + ex.getMessage());
            return new ResponseEntity<>("5 . ERROR asignarLecciones : " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = POST, value = "/calcularAvanceCurso", produces="application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> calcularAvanceCurso(@RequestBody String requestBody,
                                                 @RequestAttribute("prefijo") String prefijo,
                                                 @RequestAttribute("versionAplicacion") Integer versionAplicacion,
                                                 @RequestAttribute("idPublico") String idPublico,
                                                 @RequestAttribute("urlApiWrite") String urlApiWrite,
                                                 @RequestAttribute("urlApiRead") String urlApiRead) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            return new ResponseEntity<>(
                    amautaService.calcularAvanceCurso(requestBody, prefijo, versionAplicacion, idPublico, urlApiWrite, urlApiRead),
                    headers,
                    HttpStatus.OK
            );
        } catch (MyErrorListException myEx) {
            if (myEx.getErrors() != null && myEx.getErrors().size() > 0) {
                logger.info("error ws calcularAvanceCurso: " + myEx.getErrors().get(0).getMessage());
            }
            return new ResponseEntity<>(new JSONSerializer()
                    .exclude("*.class")
                    .exclude("cause")
                    .exclude("localizedMessage")
                    .exclude("stackTraceDepth")
                    .serialize(myEx.getErrors()), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            logger.info("error ws calcularAvanceCurso: ex: " + ex.getMessage());
            return new ResponseEntity<>("5 . ERROR calcularAvanceCurso : " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = POST, value = "/calcularAvanceCursoFeedback", produces="application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> calcularAvanceCursoFeedback(@RequestBody String requestBody,
                                                 @RequestAttribute("prefijo") String prefijo,
                                                 @RequestAttribute("versionAplicacion") Integer versionAplicacion,
                                                 @RequestAttribute("idPublico") String idPublico,
                                                 @RequestAttribute("urlApiWrite") String urlApiWrite,
                                                 @RequestAttribute("urlApiRead") String urlApiRead) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            return new ResponseEntity<>(
                    amautaService.calcularAvanceCursoFeedBack(requestBody, prefijo, versionAplicacion, idPublico, urlApiWrite, urlApiRead),
                    headers,
                    HttpStatus.OK
            );
        } catch (MyErrorListException myEx) {
            if (myEx.getErrors() != null && myEx.getErrors().size() > 0) {
                logger.info("error ws calcularAvanceCursoFeedback: " + myEx.getErrors().get(0).getMessage());
            }
            return new ResponseEntity<>(new JSONSerializer()
                    .exclude("*.class")
                    .exclude("cause")
                    .exclude("localizedMessage")
                    .exclude("stackTraceDepth")
                    .serialize(myEx.getErrors()), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            logger.info("error ws calcularAvanceCursoFeedback: ex: " + ex.getMessage());
            return new ResponseEntity<>("5 . ERROR calcularAvanceCursoFeedback : " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = POST, value = "/asignarEvaluacionTareas", produces="application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> asignarEvaluacionTareas(@RequestBody String requestBody,
                                                         @RequestAttribute("prefijo") String prefijo,
                                                         @RequestAttribute("versionAplicacion") Integer versionAplicacion,
                                                         @RequestAttribute("idPublico") String idPublico,
                                                         @RequestAttribute("urlApiWrite") String urlApiWrite,
                                                         @RequestAttribute("urlApiRead") String urlApiRead) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            return new ResponseEntity<>(
                    amautaService.asignarEvaluacionTareas(requestBody, prefijo, versionAplicacion, idPublico, urlApiWrite, urlApiRead),
                    headers,
                    HttpStatus.OK
            );
        } catch (MyErrorListException myEx) {
            if (myEx.getErrors() != null && myEx.getErrors().size() > 0) {
                logger.info("error ws asignarEvaluacionTareas: " + myEx.getErrors().get(0).getMessage());
            }
            return new ResponseEntity<>(new JSONSerializer()
                    .exclude("*.class")
                    .exclude("cause")
                    .exclude("localizedMessage")
                    .exclude("stackTraceDepth")
                    .serialize(myEx.getErrors()), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            logger.info("error ws asignarEvaluacionTareas: ex: " + ex.getMessage());
            return new ResponseEntity<>("5 . ERROR asignarEvaluacionTareas : " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = GET, value = "/ejecutarCronAsignarFeedbackAdmin", produces="application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> ejecutarCronProgExternos() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            return new ResponseEntity<>(
                    amautaService.asignarFeedAdminCron(),
                    headers,
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>((MultiValueMap<String, String>) ex, BAD_REQUEST);
        }
    }

    @RequestMapping(method = GET, value = "/ejecutarCronNotificarEstudiantes", produces="application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> ejecutarCronNotificarEstudiantes() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            return new ResponseEntity<>(
                    amautaService.notificarCorreoEstudianteLeccionCron(),
                    headers,
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>((MultiValueMap<String, String>) ex, BAD_REQUEST);
        }
    }

    @RequestMapping(method = GET, value = "/ejecutarCronAprobarFeedbackAdmin", produces="application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> testAprobar() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            return new ResponseEntity<>(
                    amautaService.aprobarFeedAdminCron(),
                    headers,
                    HttpStatus.OK
            );
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>((MultiValueMap<String, String>) ex, BAD_REQUEST);
        }
    }

    @RequestMapping(method = POST, value = "/actualizarEstadoEstudianteLeccion", produces="application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> actualizarEstadoWFEstudianteLeccion(@RequestBody String requestBody,
                                                     @RequestAttribute("prefijo") String prefijo,
                                                     @RequestAttribute("versionAplicacion") Integer versionAplicacion,
                                                     @RequestAttribute("idPublico") String idPublico,
                                                     @RequestAttribute("urlApiWrite") String urlApiWrite,
                                                     @RequestAttribute("urlApiRead") String urlApiRead) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            return new ResponseEntity<>(
                    amautaService.actualizarEstadoEstudianteLeccion(requestBody, prefijo, versionAplicacion, idPublico, urlApiWrite, urlApiRead),
                    headers,
                    HttpStatus.OK
            );
        } catch (MyErrorListException myEx) {
            if (myEx.getErrors() != null && myEx.getErrors().size() > 0) {
                logger.info("error ws actualizarEstadoWFEstudianteLeccion: " + myEx.getErrors().get(0).getMessage());
            }
            return new ResponseEntity<>(new JSONSerializer()
                    .exclude("*.class")
                    .exclude("cause")
                    .exclude("localizedMessage")
                    .exclude("stackTraceDepth")
                    .serialize(myEx.getErrors()), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            logger.info("error ws asignarEvaluacionTareas: ex: " + ex.getMessage());
            return new ResponseEntity<>("5 . ERROR asignarEvaluacionTareas : " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(method = POST, value = "/calcularAvanceRespuestaFeedback", produces="application/json;charset=UTF-8")
    @ResponseBody
    public ResponseEntity<?> calcularAvanceRespuestaFeedback(@RequestBody String requestBody,
                                                         @RequestAttribute("prefijo") String prefijo,
                                                         @RequestAttribute("versionAplicacion") Integer versionAplicacion,
                                                         @RequestAttribute("idPublico") String idPublico,
                                                         @RequestAttribute("urlApiWrite") String urlApiWrite,
                                                         @RequestAttribute("urlApiRead") String urlApiRead) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            return new ResponseEntity<>(
                    amautaService.calcularAvanceRespuestaFeedBack(requestBody, prefijo, versionAplicacion, idPublico, urlApiWrite, urlApiRead),
                    headers,
                    HttpStatus.OK
            );
        } catch (MyErrorListException myEx) {
            if (myEx.getErrors() != null && myEx.getErrors().size() > 0) {
                logger.info("error ws calcularAvanceRespuestaFeedback: " + myEx.getErrors().get(0).getMessage());
            }
            return new ResponseEntity<>(new JSONSerializer()
                    .exclude("*.class")
                    .exclude("cause")
                    .exclude("localizedMessage")
                    .exclude("stackTraceDepth")
                    .serialize(myEx.getErrors()), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            logger.info("error ws calcularAvanceRespuestaFeedback: ex: " + ex.getMessage());
            return new ResponseEntity<>("5 . ERROR calcularAvanceRespuestaFeedback : " + ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
