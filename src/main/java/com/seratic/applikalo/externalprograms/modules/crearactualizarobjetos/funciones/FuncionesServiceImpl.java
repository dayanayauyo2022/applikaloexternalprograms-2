package com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.funciones;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.stereotype.Service;

import static com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.utils.FuncionesUtils.*;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.Utils.ejecutarScript;

@Service
public class FuncionesServiceImpl implements FuncionesService {

    @Override
    public String ejecutarFuncionJavascript(String script, JsonNode listaObjetoWf, String nomObjetoBase) {
        try {
            if (script != null && !script.isEmpty()) {
                if (script.contains("// Describe esta función...")) {
                    script = script.replace("// Describe esta función...", " ");
                }
                String variables = "";
                if (script.startsWith("var ")) {
                    // extraer variables:
                    String[] variablesLst = extraerVariables(script);
                    String objetoVariableWfCad = obtenerVariableWf(listaObjetoWf, nomObjetoBase, variablesLst);
                    String otrasVariablesCad = obtenerOtrasVariables(variablesLst, nomObjetoBase);
                    variables = "var ";
                    if (objetoVariableWfCad != null && !objetoVariableWfCad.isEmpty()) {
                        variables += " wf_" + nomObjetoBase + " = " + objetoVariableWfCad;
                        if (otrasVariablesCad != null && !otrasVariablesCad.isEmpty()) {
                            variables += ", " + otrasVariablesCad + ";";
                        } else {
                            variables += ";";
                        }
                    } else if (otrasVariablesCad != null && !otrasVariablesCad.isEmpty()) {
                        variables += otrasVariablesCad + ";";
                    } else {
                        variables = "";
                    }
                }
                String scriptResult;
                if (!variables.isEmpty()) {
                    // reemplazar vairiables en script:
                    scriptResult = reemplazarVariablesWf(script, variables);
                } else {
                    scriptResult = script;
                }
                // reemplar window.alert:
                scriptResult = reemplazarRetornos(scriptResult);
                String result = ejecutarScript(scriptResult);
                return result;
            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }
}
