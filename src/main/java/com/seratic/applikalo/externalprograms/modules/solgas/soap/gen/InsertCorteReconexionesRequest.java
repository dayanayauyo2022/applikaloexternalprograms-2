//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.7 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2021.11.18 a las 03:11:44 PM COT 
//
package com.seratic.applikalo.externalprograms.modules.solgas.soap.gen;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Clase Java para anonymous complex type.
 *
 * <p>
 * El siguiente fragmento de esquema especifica el contenido que se espera que
 * haya en esta clase.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="token" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="corteyreconexiones" type="{http://www.seratic.com/applikalo/externalprograms/modules/solgas/soap/gen}corteyreconexion" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 *
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "token",
    "corteyreconexiones"
})
@XmlRootElement(name = "insertCorteReconexionesRequest")
public class InsertCorteReconexionesRequest {

    @XmlElement(required = true)
    protected String token;
    @XmlElement(required = true)
    protected List<Corteyreconexion> corteyreconexiones;

    /**
     * Obtiene el valor de la propiedad token.
     *
     * @return possible object is {@link String }
     *
     */
    public String getToken() {
        return token;
    }

    /**
     * Define el valor de la propiedad token.
     *
     * @param value allowed object is {@link String }
     *
     */
    public void setToken(String value) {
        this.token = value;
    }

    /**
     * Gets the value of the corteyreconexiones property.
     *
     * <p>
     * This accessor method returns a reference to the live list, not a
     * snapshot. Therefore any modification you make to the returned list will
     * be present inside the JAXB object. This is why there is not a
     * <CODE>set</CODE> method for the corteyreconexiones property.
     *
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCorteyreconexiones().add(newItem);
     * </pre>
     *
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Corteyreconexion }
     *
     *
     */
    public List<Corteyreconexion> getCorteyreconexiones() {
        if (corteyreconexiones == null) {
            corteyreconexiones = new ArrayList<Corteyreconexion>();
        }
        return this.corteyreconexiones;
    }

}
