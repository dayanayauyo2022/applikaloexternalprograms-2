package com.seratic.applikalo.externalprograms.modules.security;

import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component("authorizationFilter")
public class AuthorizationInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        try {
            if (request.getRequestURI().startsWith("/integracion/ejecutarCronProgExternos") ||
                    request.getRequestURI().startsWith("/amauta/ejecutarCronAsignarFeedbackAdmin") ||
                    request.getRequestURI().startsWith("/amauta/ejecutarCronNotificarEstudiantes") || 
                    request.getRequestURI().startsWith("/amauta/ejecutarCronAprobarFeedbackAdmin") ||
                    request.getRequestURI().startsWith("/solgas/pruebaMedidor")) {
                return true;
            } else {
                String token = getTokenFromRequest(request);
                DataToken dataToken = TokenManager.getDataSourceInfo(token);
                if (dataToken != null) {
                    request.setAttribute("prefijo", dataToken.getPrefijo());
                    request.setAttribute("versionAplicacion", dataToken.getVersionAplicacion());
                    request.setAttribute("idPublico", dataToken.getIdPublico());
                    request.setAttribute("urlApiWrite", dataToken.getUrlApiWrite());
                    request.setAttribute("urlApiRead", dataToken.getUrlApiRead());
                    return true;
                } else {
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    response.setContentType("application/json");
                    response.getWriter().flush();
                    response.getWriter().close();
                    return false;
                }
            }
        } catch (Exception es) {
            es.printStackTrace();
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.setContentType("application/json");
            response.getWriter().flush();
            response.getWriter().close();
            return false;
        }
    }

    private String getTokenFromRequest(ServletRequest request) {
        return ((HttpServletRequest) request).getHeader("token");
    }
}
