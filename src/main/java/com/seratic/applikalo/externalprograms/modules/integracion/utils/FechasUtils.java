package com.seratic.applikalo.externalprograms.modules.integracion.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import static com.seratic.applikalo.externalprograms.modules.integracion.Constants.GMT_TIPO;

public class FechasUtils {
    private static final String FORMATO_FECHA_API = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    private static final String FORMATO_FECHA_NOTIFICACIONES = "yyyy-MM-dd'T'HH:mm:ss.SSS";
    private static final String FORMATO_SIN_HORA = "yyyy-MM-dd";
    private static final String FORMATO_FECHA_HORA_CONVERT = "dd MMMM yyyy hh:mm a";

    /*Método para obtener fecha a partir de una cadena sin hora*/
    public static final Date getFechaByCadenaSinHora(String fechaCadena) {
        try {
            Date fecha = new SimpleDateFormat(FORMATO_SIN_HORA).parse(fechaCadena);
            return fecha;
        } catch (Exception e) {
            return null;
        }
    }

    /*Método para obtener fecha a partir de una cadena*/
    public static final Date getFechaByCadenaApi(String fechaCadena) {
        try {
            String formato = FORMATO_FECHA_NOTIFICACIONES;
            if (fechaCadena.toUpperCase().contains("Z")) {
                formato = FORMATO_FECHA_API;
            }
            Date fecha = new SimpleDateFormat(formato).parse(fechaCadena);
            return fecha;
        } catch (Exception e) {
            return null;
        }
    }

    /*Método para obtener fecha a partir de una cadena con formato Z o no*/
    public static final Date getFechaByCadenaApiGenerico(String fechaCadena) {
        try {
            String formato = FORMATO_FECHA_NOTIFICACIONES;
            if (fechaCadena.toUpperCase().contains("Z")) {
                formato = FORMATO_FECHA_API;
            }
            Date fecha = new SimpleDateFormat(formato).parse(fechaCadena);
            return fecha;
        } catch (Exception e) {
            return null;
        }
    }

    /*Método para restar un dia a una fecha enviad como parámetro*/
    public static Date getFechaDiaAnterior(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        return cal.getTime();
    }

    /*Método para sumar x dias a una fecha enviad como parámetro*/
    public static Date sumarDiasAFecha(Date date, int numDias) {
        try {
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.DAY_OF_MONTH, numDias);
            return cal.getTime();
        } catch (Exception e) {
            return null;
        }
    }

    /*Método para sumar restar x horas a una fecha enviad como parámetro*/
    public static final Date sumarRestarHoras(String fechaCadena, int numHoras) {
        try {
            Date fecha = new SimpleDateFormat(FORMATO_FECHA_API).parse(fechaCadena);
            Calendar cal = Calendar.getInstance();
            cal.setTime(fecha);
            cal.add(Calendar.HOUR_OF_DAY, numHoras);
            return cal.getTime();
        } catch (Exception e) {
            return null;
        }
    }

    /*Método para sumar restar x horas a una fecha enviad como parámetro y el valor de gmt, se convierte a numero y se llama al metodo sumarRestarHoras*/
    public static final Date sumarRestarHorasByGMT(String fechaCadena, String gmt) {
        try {
            Integer numHoras = Integer.parseInt(gmt.replace(GMT_TIPO, ""));
            return sumarRestarHoras(fechaCadena, numHoras);
        } catch (Exception e) {
            return null;
        }
    }

    public static final String getTiempoTranscurrido(Date fechaInicio, Date fechaFin) {
        try {
            long diff = fechaFin.getTime() - fechaInicio.getTime();
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000);

            String horas = "" + diffHours;
            String minutos = "" + diffMinutes;
            String segundos = "" + diffSeconds;

            if (horas.length() == 1) {
                horas = "0" + horas;
            }
            if (minutos.length() == 1) {
                minutos = "0" + minutos;
            }
            if (segundos.length() == 1) {
                segundos = "0" + segundos;
            }
            String tiempoFormat = horas + ":" + minutos + ":" + segundos;
            return tiempoFormat;
        } catch (Exception e) {
            return "";
        }
    }

    public static final String formatDateConSinTimeZoneSave(Date fecha, boolean mantenerSinTimeZone) {
        try {
            DateFormat fechaFormat = new SimpleDateFormat(FORMATO_FECHA_API);
            if (!mantenerSinTimeZone) {
                fechaFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            }
            String fechaResult = fechaFormat.format(fecha);
            return fechaResult;
        } catch (Exception e) {
            return null;
        }
    }

    public static final String formatDateSinHora(Date fecha) {
        try {
            DateFormat fechaFormat = new SimpleDateFormat(FORMATO_SIN_HORA);
            String fechaResult = fechaFormat.format(fecha);
            return fechaResult;
        } catch (Exception e) {
            return null;
        }
    }

    public static final Date getDateSinHoraByDate(Date fecha) {
        try {
            DateFormat fechaFormat = new SimpleDateFormat(FORMATO_SIN_HORA);
            String fechaCadena = fechaFormat.format(fecha);
            Date dateResult = fechaFormat.parse(fechaCadena);
            return dateResult;
        } catch (Exception e) {
            return null;
        }
    }

    /*Método para calcular los dias de diferencia entre dos fechas*/
    public static final Integer getDiasDiferencia(Date fechaInicio, Date fechaFin) {
        try {
            Integer dias = (int) ((fechaFin.getTime()-fechaInicio.getTime())/86400000);
            return dias;
        } catch (Exception e) {
            return null;
        }
    }

    public static final String getFechaHoraByCadena(String fecha) {
        SimpleDateFormat formatter = new SimpleDateFormat(FORMATO_FECHA_API, new Locale("es", "PE"));
        try {
            Date date = formatter.parse(fecha);
            DateFormat fechaFormat = new SimpleDateFormat(FORMATO_FECHA_HORA_CONVERT, new Locale("es", "PE"));
            return fechaFormat.format(date);
        } catch (ParseException e) {
        }
        return "";
    }

    public static final String getFechaHoraByDate(Date fecha) {
        try {
            DateFormat fechaFormat = new SimpleDateFormat(FORMATO_FECHA_HORA_CONVERT, new Locale("es", "PE"));
            return fechaFormat.format(fecha);
        } catch (Exception e) {
        }
        return "";
    }
}
