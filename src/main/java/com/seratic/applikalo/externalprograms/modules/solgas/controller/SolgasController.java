package com.seratic.applikalo.externalprograms.modules.solgas.controller;

import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;
import com.seratic.applikalo.externalprograms.modules.solgas.service.SolgasService;
import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author MONDRAGON
 */
@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.POST})
@RequestMapping(value = "/solgas")
public class SolgasController {

    private static final Logger logger = LoggerFactory.getLogger(SolgasController.class);

    @Autowired
    SolgasService solgasService;

    @RequestMapping(method = POST, value = "/calcularPromedio", produces = MediaType.APPLICATION_PDF_VALUE)
    @ResponseBody
    public ResponseEntity<?> calcularPromedio(@RequestBody String requestBody,
            @RequestAttribute("prefijo") String prefijo,
            @RequestAttribute("versionAplicacion") Integer versionAplicacion,
            @RequestAttribute("idPublico") String idPublico,
            @RequestAttribute("urlApiWrite") String urlApiWrite,
            @RequestAttribute("urlApiRead") String urlApiRead) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        try {
            logger.info("Entro ejecución PExt -> " + requestBody);
            return new ResponseEntity<>(solgasService.calcularPromedio(requestBody, prefijo, versionAplicacion,
                    idPublico, urlApiWrite, urlApiRead), headers,
                    HttpStatus.OK);
        } catch (MyErrorListException myEx) {
            logger.info("CONTROLLER : " + serializeErrors(myEx.getErrors()));
            return new ResponseEntity<>(serializeErrors(myEx.getErrors()), headers, HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            logger.info("Fallo ejecución PExt -> ", ex);
            return new ResponseEntity<>((MultiValueMap<String, String>) ex, BAD_REQUEST);
        }
    }

    private String serializeErrors(Object object) {
        return new JSONSerializer()
                .exclude("*.class")
                .exclude("cause")
                .exclude("localizedMessage")
                .exclude("stackTraceDepth")
                .serialize(object);
    }

}
