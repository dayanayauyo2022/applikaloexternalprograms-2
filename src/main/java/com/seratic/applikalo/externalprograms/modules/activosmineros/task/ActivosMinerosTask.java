package com.seratic.applikalo.externalprograms.modules.activosmineros.task;

import com.fasterxml.jackson.databind.JsonNode;
import com.seratic.applikalo.externalprograms.modules.activosmineros.service.ArmarNotificacion;
import com.seratic.applikalo.externalprograms.modules.activosmineros.repositorio.ObtenerDataImp;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static com.seratic.applikalo.externalprograms.modules.activosmineros.utils.ConstantesLlave.KEYFECHAFUERADEPLAZO;

@Component
public class ActivosMinerosTask {

    @Autowired
    private ArmarNotificacion armarNotificacion;
    @Autowired
    private ObtenerDataImp dataRepository;
    @Value(value = "${activos-mineros.envio-notificacion.proyecto-estado-id}")
    private Integer idEstadoProyecto;
    private static final DateTimeFormatter inputFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);

    @Scheduled(cron = "${activos-mineros.envio-notificacion.cron}")
    public void notificarProyectos(){
        List<JsonNode> retorno = new ArrayList<>();
        LocalDate currentDate = LocalDate.now();
        dataRepository.obtenerProyectos().stream().forEach(proyecto -> {
            if (!Objects.equals(proyecto.get(KEYFECHAFUERADEPLAZO).asText(), "null") && !Objects.equals(proyecto.get(KEYFECHAFUERADEPLAZO).asText(), "") && Objects.nonNull(proyecto.get(KEYFECHAFUERADEPLAZO).asText()) && ChronoUnit.DAYS.between(LocalDate.parse(proyecto.get(KEYFECHAFUERADEPLAZO).asText(), inputFormat), currentDate) >= 0 && !idEstadoProyecto.equals(proyecto.get("estado_proyecto").get("id").asInt())) {
                retorno.add(proyecto);
            }
        });
        if (retorno.isEmpty()) {
            LogFactory.getLog(ActivosMinerosTask.class).info("No hay proyectos Fuera de plazo");
        } else {
            armarNotificacion.armarDataNotificacionProyectosFueraDePlazo(retorno, currentDate);
        }
    }

    @Scheduled(cron = "${activos-mineros.envio-notificacion.cron}")
    public void notificacionProyectosCamposVacios() {
        armarNotificacion.armarNoitificacionProyectosCamposVacios();
    }

    @Scheduled(cron = "${activos-mineros.envio-notificacion.cron}")
    public void notificacionProyectosNoSupervisados() {
        armarNotificacion.armarNotificacionProyectosNoSupervisados();
    }
}
