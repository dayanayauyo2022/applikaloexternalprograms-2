package com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.VOs.LocationVo;
import com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.VOs.ObjetoConAtributosVO;
import com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.VOs.PointVo;
import com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.funciones.FuncionesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.utils.ConstantsObjetos.*;
import static com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.utils.UtilsObjetos.*;
import com.seratic.applikalo.externalprograms.modules.integracion.IntegracionRepository;
import com.seratic.applikalo.externalprograms.modules.integracion.VOs.RespuestaVO;
import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import static com.seratic.applikalo.externalprograms.modules.security.utils.TokenManagerUtil.generarTokenGetObjeto;

@Service
public class CrearActualizarObjetosServiceImpl implements CrearActualizarObjetosService {

    private static final Logger logger = LoggerFactory.getLogger(CrearActualizarObjetosServiceImpl.class);

    @Autowired
    private IntegracionRepository integracionRepository;

    @Autowired
    private FuncionesService funcionesService;

    @Override
    public RespuestaVO crearActualizarObjeto(boolean isCreacion, String objetoRequest, String prefijo, Integer versionApp,
            String idPublico, String urlWrite, String urlRead) {
        MyErrorListException errList = new MyErrorListException();
        try {
            boolean programaUbicacion = false;
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoRequestJson = mapper.readTree(objetoRequest);
            JsonNode objetoBase = objetoRequestJson.get("objeto");
            String nombreObjetoBase = objetoRequestJson.get("nombreObjeto").asText();
            String idObjetoBase = objetoBase.get("id").asText();
            boolean mantenerSinTimeZone = true;
            if (objetoRequestJson.get("mantenerSinTimeZone") != null) {
                mantenerSinTimeZone = objetoRequestJson.get("mantenerSinTimeZone").asBoolean();
            }
            DataToken dataTokenApi = new DataToken(prefijo, idPublico, versionApp, urlWrite, urlRead);

            JsonNode programaExterno = objetoRequestJson.get("programaExterno");
            JsonNode atributosCampos = programaExterno.get("atributosCampos");

            String nombreCampoUpdateInsert;
            if (isCreacion) {
                nombreCampoUpdateInsert = "valorCrear";
            } else {
                nombreCampoUpdateInsert = "valorActualizar";
            }
            if (atributosCampos != null && atributosCampos.size() > 0) {
                // buscar lista de objetos wf:
                List<String> listaCamposWf = buscarCamposWf(atributosCampos, nombreObjetoBase);
                logger.info("** listaCamposWf ***" + listaCamposWf.toString());
                JsonNode listaObjetoWf = obtenerListaObjetoWf(nombreObjetoBase, idObjetoBase, listaCamposWf, dataTokenApi);

                List<ObjetoConAtributosVO> lstObjetosConAtrib = new ArrayList<>();
                for (JsonNode atribCampo : atributosCampos) {
                    if (atribCampo.get("tipoCampo") != null && atribCampo.get("tipoCampo").get("id") != null
                            && atribCampo.get("tipoCampo").get("id").asText().equals(TIPO_CAMPO_OBJETO)) {
                        String nombreObjeto = atribCampo.get(nombreCampoUpdateInsert).asText();
                        // volver a buscar en toda la lista campos de este objeto, segun su jerarquia:
                        List<JsonNode> listaCamposDeObjeto = new ArrayList<>();
                        for (JsonNode campoDeObjeto : atributosCampos) {
                            String nombreCampo = campoDeObjeto.get(nombreCampoUpdateInsert).asText();
                            if (nombreCampo.startsWith(nombreObjeto + ".")) {
                                listaCamposDeObjeto.add(campoDeObjeto);
                            }
                        }
                        ObjetoConAtributosVO objetoConAtributosVO = new ObjetoConAtributosVO(atribCampo, listaCamposDeObjeto);
                        lstObjetosConAtrib.add(objetoConAtributosVO);
                    }
                }
                for (ObjetoConAtributosVO objConAtt : lstObjetosConAtrib) {
                    try {
                        JsonNode objeto = objConAtt.getObjeto();
                        String nombreObjeto = objeto.get(nombreCampoUpdateInsert).asText();

                        JsonNode camposObjetoUpdteInsert = mapper.createObjectNode();
                        LocationVo location = new LocationVo();
                        for (JsonNode campoObj : objConAtt.getListaCampos()) {
                            if (campoObj.get("tipoCampo") != null && campoObj.get("tipoCampo").get("id") != null
                                    && campoObj.get("tipoConfiguracion") != null) {

                                if (campoObj.get("programa") != null
                                        && campoObj.get("programa").get("nombre") != null
                                        && campoObj.get("programa").get("nombre").asText().equals("Verificacion de ubicacion")) {
                                    programaUbicacion = true;
                                }

                                String tipoCampo = campoObj.get("tipoCampo").get("id").asText();
                                JsonNode tipoConfig = campoObj.get("tipoConfiguracion");
                                String valorConfigurar = campoObj.get("valorConfigurar").asText();
                                boolean isRelacion = tipoCampo.equals(TIPO_CAMPO_RELACION);

                                String valorCampo = "";
                                JsonNode valorCampoWfJson = null;
                                boolean isCampoWf = false;
                                if (tipoConfig.get("id").asText().equals(TIPO_CONFIG_WF)) {
                                    valorCampoWfJson = obtenerValorApartirDeWf(tipoConfig, valorConfigurar, listaObjetoWf, isRelacion);
                                    isCampoWf = true;
                                } else {
                                    valorCampo = obtenerValorApartirDeOtros(tipoCampo, tipoConfig, valorConfigurar, listaObjetoWf,
                                            isRelacion, campoObj, mantenerSinTimeZone, nombreObjetoBase);
                                }

                                String nombreCampoRuta = campoObj.get(nombreCampoUpdateInsert).asText();
                                String nombreCampo = nombreCampoRuta.split("\\.")[1];

                                if (isRelacion) {
                                    JsonNode objRel = mapper.createObjectNode();
                                    if (isCampoWf) {
                                        if (!valorCampoWfJson.isObject()) {
                                            ((ObjectNode) objRel).put("id", valorCampoWfJson.asText());
                                        } else {
                                            ((ObjectNode) objRel).put("id", valorCampoWfJson.get("id").asText());
                                        }
                                    } else {
                                        ((ObjectNode) objRel).put("id", valorCampo);
                                    }
                                    ((ObjectNode) camposObjetoUpdteInsert).put(nombreCampo, objRel);
                                } else {
                                    if (isCampoWf) {
                                        // si es campo numero convertir a numero de lo contrario se guarda en el tipo que tenga el objeto json
                                        if (valorCampoWfJson != null && valorCampoWfJson.isInt()) {
                                            ((ObjectNode) camposObjetoUpdteInsert).put(nombreCampo, valorCampoWfJson.asInt());
                                        } else {
                                            ((ObjectNode) camposObjetoUpdteInsert).put(nombreCampo, valorCampoWfJson);
                                        }
                                    } else {
                                        ((ObjectNode) camposObjetoUpdteInsert).put(nombreCampo, valorCampo);
                                    }
                                }

                                if (programaUbicacion && campoObj.get("nombre").asText().equals("campo a actualizar")) {
                                    location.setFieldToUpdate(nombreCampo);
                                }
                                if (programaUbicacion && campoObj.get("nombre").asText().equals("ubicación de referencia")) {
                                    if (valorCampoWfJson != null && valorCampoWfJson.get("lat") != null && valorCampoWfJson.get("lng") != null) {
                                        PointVo pointTemp = new PointVo(valorCampoWfJson.get("lat").asDouble(), valorCampoWfJson.get("lng").asDouble());
                                        location.setLocationOne(pointTemp);
                                    }
                                }
                                if (programaUbicacion && campoObj.get("nombre").asText().equals("ubicación de marcacion")) {
                                    if (valorCampoWfJson != null && valorCampoWfJson.get("lat") != null && valorCampoWfJson.get("lng") != null) {
                                        PointVo pointTemp = new PointVo(valorCampoWfJson.get("lat").asDouble(), valorCampoWfJson.get("lng").asDouble());
                                        location.setLocationTwo(pointTemp);
                                    }
                                }
                                if (programaUbicacion && campoObj.get("nombre").asText().equals("radio")) {
                                    if (valorCampo != null) {
                                        location.setRadio(Integer.parseInt(valorCampo));
                                    }
                                }
                            }
                        }
                        //Insertar o Actualizar objeto:
                        if (isCreacion) {
                            String key = generarKeyInsertObjeto(versionApp);
                            ((ObjectNode) camposObjetoUpdteInsert).put("key", key);
                            String bodyCrearObjeto = armarPeticionCrearObjeto(camposObjetoUpdteInsert, nombreObjeto);
                            logger.info("**PROG CREAR OBJETO insertRegisterPost BODY: " + bodyCrearObjeto);
                            ResponseEntity<String> respInsertObject = integracionRepository.insertApi(dataTokenApi, bodyCrearObjeto);
                            JsonNode nodoRespInsertObject = mapper.readTree(respInsertObject.getBody());
                            logger.info("**PROG CREAR OBJETO insertRegisterPost RESPUESTA: " + nodoRespInsertObject.toString());
                        } else {
                            String tipoCampo = objeto.get("tipoCampo").get("id").asText();

                            String idObjetotoUpdate = "";
                            JsonNode idObjetotoUpdateWfJson = null;
                            boolean isCampoObjWf = false;
                            if (objeto.get("tipoConfiguracion").get("id").asText().equals(TIPO_CONFIG_WF)) {
                                isCampoObjWf = true;
                                boolean isRelationTemp = objeto.get("tipoCampo").get("id").asText().equals(TIPO_CAMPO_RELACION);
                                idObjetotoUpdateWfJson = obtenerValorApartirDeWf(objeto.get("tipoConfiguracion"), objeto.get("valorConfigurar").asText(), listaObjetoWf, isRelationTemp);
                            } else {
                                idObjetotoUpdate = obtenerValorApartirDeOtros(tipoCampo, objeto.get("tipoConfiguracion"), objeto.get("valorConfigurar").asText(),
                                        listaObjetoWf, true, objeto, mantenerSinTimeZone, nombreObjetoBase);
                            }
                            logger.info("** objeto ***" + objeto.toString());
                            logger.info("** listaObjetoWf ***" + listaObjetoWf.toString());
                            logger.info("** nombreObjetoBase ***" + nombreObjetoBase);
                            logger.info("** isCampoObjWf ***" + isCampoObjWf);
                            logger.info("** idObjetotoUpdateWfJson ***" + idObjetotoUpdateWfJson);
                            logger.info("** idObjetotoUpdate ***" + idObjetotoUpdate);
                            logger.info("** idObjetoBase ***" + idObjetoBase);
                            if (isCampoObjWf) {
                                ((ObjectNode) camposObjetoUpdteInsert).put("id", idObjetotoUpdateWfJson != null ? idObjetotoUpdateWfJson.asText() : idObjetoBase);
                            } else {
                                ((ObjectNode) camposObjetoUpdteInsert).put("id", idObjetotoUpdate);
                            }

                            String bodyUpdate;

                            if (programaUbicacion) {
                                boolean isOnRadio;
                                if (location.getLocationOne() != null && location.getLocationTwo() != null) {
                                    double distance = integracionRepository.getDistance(dataTokenApi, location);
                                    if (distance > location.getRadio()) {
                                        isOnRadio = false;
                                    } else {
                                        isOnRadio = true;
                                    }
                                } else {
                                    isOnRadio = false;
                                }
                                JsonNode fieldsDistance = mapper.createObjectNode();
                                ((ObjectNode) fieldsDistance).put("id", idObjetoBase);
                                ((ObjectNode) fieldsDistance).put(location.getFieldToUpdate(), isOnRadio);
                                bodyUpdate = armarPeticionActualizarObjeto(fieldsDistance, nombreObjeto);
                            } else {
                                bodyUpdate = armarPeticionActualizarObjeto(camposObjetoUpdteInsert, nombreObjeto);
                            }
                            logger.info("**PROG CREAR OBJETO update BODY: " + bodyUpdate);
                            ResponseEntity<String> respUpdateObject = integracionRepository.updateServicioUpdateApigo(dataTokenApi, bodyUpdate, true, true, true);
                            JsonNode nodoRespUpdateObject = mapper.readTree(respUpdateObject.getBody());
                            logger.info("**PROG CREAR OBJETO update RESPUESTA: " + nodoRespUpdateObject.toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        logger.info("error al crear objeto: " + e.getMessage());
                    }
                }
            }
        } catch (MyErrorListException e) {
            e.printStackTrace();
            if (e.getErrors() != null) {
                errList.addErrorList(e.getErrors());
            }
            if (errList.getErrors() == null || (errList.getErrors() != null && errList.getErrors().size() == 0)) {
                errList.addError("412", "Error creacion objeto", e.getMessage());
            }
            logger.info("error al crear objeto: " + errList.getErrors().get(0).getMessage());
            throw errList;
        } catch (Exception e) {
            errList.addError("412", "Error creacion de objeto", e.getMessage());
            logger.info("Error creacion objeto: " + e.getMessage());
        }

        if (errList.getErrors().size() > 0) {
            logger.info("error creacion objeto: " + errList.getErrors().get(0).getMessage());
            throw errList;
        } else {
            logger.info("****EXITO. CREACION OBJETOS ***");
            RespuestaVO response = new RespuestaVO();
            response.setCode("200");
            response.setMessage("success");
            return response;
        }
    }

    public String obtenerValorApartirDeOtros(String tipoCampo, JsonNode tipoConfig, String valorConfigurar, JsonNode listaObjetoWf,
            boolean isRelacion, JsonNode configCampo, boolean mantenerSinTimeZone, String nomObjetoBase) {

        if (isRelacion && tipoConfig.get("id").asText().equals(TIPO_CONFIG_ESTATICO)
                && (configCampo.get("valorConfigurarId") != null && !configCampo.get("valorConfigurarId").isNull())) {
            return configCampo.get("valorConfigurarId").asText();
        } else if (tipoConfig.get("id").asText().equals(TIPO_CONFIG_ESTATICO)) {
            return obtenerValorEstatico(tipoCampo, valorConfigurar, mantenerSinTimeZone);
        } else if (tipoConfig.get("id").asText().equals(TIPO_CONFIG_FUNCION)) {
            if (configCampo.get("valorFuncionJs") != null && !configCampo.get("valorFuncionJs").isNull() && configCampo.get("valorFuncionJs").asText() != null) {
                return funcionesService.ejecutarFuncionJavascript(configCampo.get("valorFuncionJs").asText(), listaObjetoWf, nomObjetoBase);
            } else {
                return "";
            }
        } else if (tipoConfig.get("id").asText().equals(TIPO_CONFIG_RANGO_ESTADO)) {
            String duracion = "";
            if (configCampo.get("valorEstadoInicial") != null && configCampo.get("valorEstadoFinal") != null) {
                duracion = getDiferenciaEstados(configCampo.get("valorEstadoInicial").asText(), configCampo.get("valorEstadoFinal").asText(), listaObjetoWf);
            }
            return duracion;
        } else {
            return "";
        }
    }

    public JsonNode obtenerValorApartirDeWf(JsonNode tipoConfig, String valorConfigurar, JsonNode listaObjetoWf,
            boolean isRelacion) {
        if (tipoConfig.get("id").asText().equals(TIPO_CONFIG_WF)) {
            JsonNode valor = obtenerValorCampoWf(listaObjetoWf, valorConfigurar, isRelacion);
            return valor;
        } else {
            return null;
        }
    }

    public JsonNode obtenerListaObjetoWf(String nombreObjeto, String idObjeto, List<String> listaCamposWf, DataToken dataTokenApiRead) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            listaCamposWf.add("nodo");
            listaCamposWf.add(nombreObjeto + ".id");
            listaCamposWf.add("fecha_creacion");
            String bodyGetObjeto = armarPeticionGetObjetoWfApi(nombreObjeto, idObjeto, "", listaCamposWf, true, 1, 0, 30);
            logger.info("** bodyGetObjeto ***" + bodyGetObjeto);
            String tokenEncriptApiRead = generarTokenGetObjeto(dataTokenApiRead);
            ResponseEntity<String> responseGetObjeto = integracionRepository.getObjetoApi(dataTokenApiRead, tokenEncriptApiRead, bodyGetObjeto);
            JsonNode nodoRespGetObjeto = mapper.readTree(responseGetObjeto.getBody());
            if (nodoRespGetObjeto.get("statusCode") != null && !nodoRespGetObjeto.get("statusCode").asText().equals("200")) {
                errList.addError("412", "get Tabla wf", "No fue posible obtner nodos de wf_: " + nombreObjeto + " version: " + dataTokenApiRead.getVersionAplicacion());
                throw errList;
            }
            logger.info("** nodoRespGetObjeto ***" + nodoRespGetObjeto.toString());
            JsonNode listaNodosWf = nodoRespGetObjeto.get("data");
            return listaNodosWf;
        } catch (Exception e) {
            return null;
        }
    }
}
