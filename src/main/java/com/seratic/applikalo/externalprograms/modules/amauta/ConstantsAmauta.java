package com.seratic.applikalo.externalprograms.modules.amauta;

public class ConstantsAmauta {
    public static final String KEY_PROG_ASIGNAR_LECCIONES = "ASIGLECC";
    public static final String KEY_CRON_ASIGNAR_LECCIONES = "CRONASIGLECC";

    // Prog asignar evaluaion lecciones:
    public static final String NOMBRE_TIPO_LECCION_FEEDBACK = "Evaluación escrita";
    public static final String NIVEL_1A = "1A";
    public static final String NIVEL_1B = "1B";
    public static final String NIVEL_1C = "1C";
    public static final Integer NUM_ESTUDIANTES_ASIGNAR = 5;
    public static final String NOMBRE_PERFIL_ADMIN = "Admin amauta";

    // cron asignar Admin
    public static final String ESTADO_APROBADO = "Aprobado";
    public static final String ESTADO_DESAPROBADO = "Desaprobado";
    public static final String ESTADO_FINALIZADO = "Finalizado";

    // cron notificar estudiantes conferencia:
    public static final String ASUNTO_CRON_NOTIFI = "Recordatorio Conferencia";
    public static final String BODY_CRON_NOTIFI_CONFE = "<p>Recordatorio de la conferencia: <strong>[estudiante_leccion.leccion.nombre]</strong></p>" +
            "<p>Se informa que faltan <strong>[estudiante_leccion.n_dias_faltan]</strong> dia(s) para la conferencia.</p>" +
            "<p>  Fecha: <strong>[estudiante_leccion.leccion.fecha]</strong></p>" +
            "<p>  link de acceso: <a href=\"[estudiante_leccion.leccion.link_conferencia]\" target=\"_blank\">[estudiante_leccion.leccion.link_conferencia]</a></p>" +
            "<p>Saludos cordiales</p><p> <img src=\"https://entelclientes.s3.amazonaws.com/LogosEmpresas/amauta-logo.png\" width=\"280\" height=\"80\"><br></p>";

    // calculo avances:
    public static final String TIPO_AVANCE_LECCION = "TAL";
    public static final String TIPO_AVANCE_FEEDBACK = "TAF";
    public static final String TIPO_AVANCE_REFEED = "TAR";
    public static final String TIPO_LECCION_CONFE = "Conferencia";
    public static final String TIPO_LECCION_ENCUESTA = "Encuesta";
}
