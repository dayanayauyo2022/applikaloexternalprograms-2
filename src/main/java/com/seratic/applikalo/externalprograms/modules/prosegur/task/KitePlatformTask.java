package com.seratic.applikalo.externalprograms.modules.prosegur.task;

import com.seratic.applikalo.externalprograms.modules.prosegur.endpoint.ImapMessageReceiverEndpoint;
import com.seratic.applikalo.externalprograms.modules.prosegur.service.ProsegurService;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class KitePlatformTask {

    private static final Logger logger = LoggerFactory.getLogger(ImapMessageReceiverEndpoint.class);
    @Autowired
    private ProsegurService prosegurService;
    @Value("${prosegur.mailDownReceiver.prefijo}")
    private String prefijo;
    @Value("${prosegur.mailDownReceiver.idPublico}")
    private String idPublico;
    @Value("${prosegur.mailDownReceiver.versionAplicacion}")
    private Integer versionAplicacion;
    @Value("${prosegur.mailDownReceiver.urlWriter}")
    private String urlWriter;
    @Value("${prosegur.mailDownReceiver.urlReader}")
    private String urlReader;

    @Scheduled(cron = "${prosegur.mailDownReceiver.mail.cron}")
    public void darDeBajaSims() {
        try {
            DataToken dataToken = new DataToken(prefijo, idPublico, versionAplicacion, urlWriter, urlReader);
            prosegurService.executeDownSIM(dataToken);
        } catch (Exception e) {
            logger.error("darDeBajaSims fallo", e);
        }
    }

    @Scheduled(cron = "${prosegur.kitePlatform.cronAssignSims}")
    public void asignarSims() {
        try {
            DataToken dataToken = new DataToken(prefijo, idPublico, versionAplicacion, urlWriter, urlReader);
            prosegurService.executeInstallSIM(dataToken);
        } catch (Exception e) {
            logger.error("asignarSims fallo", e);
        }
    }
}
