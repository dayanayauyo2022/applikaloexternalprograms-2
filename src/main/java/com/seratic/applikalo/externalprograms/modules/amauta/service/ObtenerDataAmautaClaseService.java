package com.seratic.applikalo.externalprograms.modules.amauta.service;

import java.util.List;
import java.util.Map;

public interface ObtenerDataAmautaClaseService {

    List<Map<String, Object>> obtenerClasesEnVivo();
    List<Map<String, Object>> obtenerEstudianteCurso();
    List<Map<String, Object>> obtenerUsuarios();
}
