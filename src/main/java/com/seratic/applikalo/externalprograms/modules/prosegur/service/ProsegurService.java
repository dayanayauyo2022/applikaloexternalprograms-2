/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seratic.applikalo.externalprograms.modules.prosegur.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.seratic.applikalo.externalprograms.modules.prosegur.dto.EstadoSimBajaDTO;
import com.seratic.applikalo.externalprograms.modules.prosegur.dto.EventoDTO;
import com.seratic.applikalo.externalprograms.modules.prosegur.dto.InstalacionDTO;
import com.seratic.applikalo.externalprograms.modules.prosegur.dto.InstalacionDownDTO;
import com.seratic.applikalo.externalprograms.modules.prosegur.dto.SimBajaDTO;
import com.seratic.applikalo.externalprograms.modules.prosegur.dto.SimDTO;
import com.seratic.applikalo.externalprograms.modules.prosegur.dto.SuscriptionDTO;
import com.seratic.applikalo.externalprograms.modules.prosegur.dto.UbicacionDTO;
import com.seratic.applikalo.externalprograms.modules.prosegur.repository.KitePlatformRepository;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import static com.seratic.applikalo.externalprograms.modules.security.utils.TokenManagerUtil.generarTokenGetObjeto;
import com.seratic.applikalo.externalprograms.objectmanager.dto.CampoDTO;
import com.seratic.applikalo.externalprograms.objectmanager.dto.FiltroDTO;
import com.seratic.applikalo.externalprograms.modules.prosegur.dto.ObjectManagerGetInstalacionResponseDTO;
import com.seratic.applikalo.externalprograms.modules.prosegur.dto.ObjectManagerGetSimBajaResponseDTO;
import com.seratic.applikalo.externalprograms.modules.prosegur.dto.SimBajaDownDTO;
import com.seratic.applikalo.externalprograms.objectmanager.dto.ObjectManagerGetRequestDTO;
import com.seratic.applikalo.externalprograms.objectmanager.dto.ObjectManagerInsertRequest;
import com.seratic.applikalo.externalprograms.objectmanager.repository.ObjectManagerRepository;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.mail.BodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author ivang
 */
@Service
public class ProsegurService {

    private static final Logger logger = LoggerFactory.getLogger(ProsegurService.class);
    private static final long MILLISECONDS_IN_MONTH = 2592000000L;
    @Value("${prosegur.kitePlatform.lifeCycleDown}")
    private String lifeCycleDown;
    @Value("${prosegur.kitePlatform.iccPrefix}")
    private String iccPrefix;
    @Value("${prosegur.kitePlatform.iccLength}")
    private int iccLength;
    @Value("${prosegur.kitePlatform.simMinMonthFromActivated}")
    private int simMinMonthFromActivated;
    @Value("${prosegur.kitePlatform.idEstadoBajaInvalido}")
    private int idEstadoBajaInvalido;
    @Value("${prosegur.kitePlatform.idEstadoBajaConfirmado}")
    private int idEstadoBajaConfirmado;
    @Value("${prosegur.kitePlatform.idEstadoBajaProgramada}")
    private int idEstadoBajaProgramada;

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private ObjectManagerRepository objectManagerRepository;
    @Autowired
    private KitePlatformRepository kitePlatformRepository;

    public boolean installSIM(SimDTO simDTO, DataToken dataToken) {
        boolean retorno = false;
        try {
            List<FiltroDTO> filtros = new ArrayList<>(1);
            filtros.add(new FiltroDTO("codigo", "=", "sim", "'" + simDTO.getIcc() + "'"));
            if (installSIM(filtros, dataToken)) {
                retorno = true;
            }
        } catch (Exception e) {
            logger.error("installSIM fallo", e);
        }
        return retorno;
    }

    private boolean installSIM(List<FiltroDTO> filtros, DataToken dataToken) {
        boolean retorno = false;
        try {
            String tokenEncriptApiRead = generarTokenGetObjeto(dataToken);
            List<CampoDTO> campos = new ArrayList<>(4);
            campos.add(new CampoDTO("id"));
            campos.add(new CampoDTO("sim.codigo"));
            campos.add(new CampoDTO("abonado"));
            campos.add(new CampoDTO("contrato"));
            campos.add(new CampoDTO("ubicacion"));
            campos.add(new CampoDTO("plataforma"));
            campos.add(new CampoDTO("enviado"));
            ObjectManagerGetRequestDTO objectManagerGetRequestDTO = new ObjectManagerGetRequestDTO("instalacion", campos);
            objectManagerGetRequestDTO.setFiltros(filtros);
            String response = objectManagerRepository.get(dataToken.getUrlApiRead(), tokenEncriptApiRead, objectManagerGetRequestDTO);
            if (response != null) {
                ObjectManagerGetInstalacionResponseDTO objectManagerGetInstalacionResponseDTO = objectMapper.readValue(response, ObjectManagerGetInstalacionResponseDTO.class);
                if (objectManagerGetInstalacionResponseDTO != null) {
                    List<InstalacionDownDTO> instalacionDownDTOs = objectManagerGetInstalacionResponseDTO.getData();
                    if ((instalacionDownDTOs != null) && (!instalacionDownDTOs.isEmpty())) {
                        retorno = true;
                        for (InstalacionDownDTO instalacionDownDTO : instalacionDownDTOs) {
                            SuscriptionDTO suscriptionDTO = new SuscriptionDTO();
                            suscriptionDTO.setIcc(instalacionDownDTO.getSim().getIcc());
                            suscriptionDTO.setCustomField1(instalacionDownDTO.getAbonado());
                            suscriptionDTO.setCustomField2(instalacionDownDTO.getContrato());
                            suscriptionDTO.setCustomField3(instalacionDownDTO.getUbicacion());
                            suscriptionDTO.setCustomField4(instalacionDownDTO.getPlataforma());
                            if (kitePlatformRepository.modifySuscription(suscriptionDTO)) {
                                InstalacionDTO instalacionDTO = new InstalacionDTO(instalacionDownDTO);
                                instalacionDTO.setEnviado(true);
                                response = objectManagerRepository.update(dataToken, instalacionDTO, false, false, false, false);
                                if (response != null) {
                                    retorno &= true;
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.error("installSIM fallo", e);
        }
        return retorno;
    }

    public boolean validateDownSIM(SimBajaDownDTO simBajaDownDTO, DataToken dataToken) {
        boolean retorno = false;
        try {
            String icc = simBajaDownDTO.getIcc();
            SimBajaDTO simBajaDTO = new SimBajaDTO(icc);
            if ((icc.length() == iccLength) && (icc.startsWith(iccPrefix))) {
                retorno = true;
            } else {
                String motivo = null;
                if (icc.length() != iccLength) {
                    motivo = "La longitud del ICC (" + icc + ") es diferente a " + iccLength;
                }
                if (!icc.startsWith(iccPrefix)) {
                    motivo = (motivo == null ? "E" : (motivo + " y e")) + "l prefijo del ICC (" + icc + ") es diferente a " + iccPrefix;
                }
                simBajaDTO.setMotivo(motivo);
            }
            simBajaDTO.setEstadoSimBaja(new EstadoSimBajaDTO(retorno ? idEstadoBajaProgramada : idEstadoBajaInvalido));
            objectManagerRepository.update(dataToken, simBajaDTO, false, false, false, false);
        } catch (Exception e) {
            logger.error("createDownSIM fallo", e);
        }
        return retorno;
    }

    public boolean createDownSIM(List<SimBajaDTO> simBajaDTOs, DataToken dataToken) {
        boolean retorno = false;
        try {
            List<SimBajaDTO> response = objectManagerRepository.insert(dataToken, new ObjectManagerInsertRequest(simBajaDTOs), false, SimBajaDTO.class);
            if (response == null) {
                logger.error("createDownSIM fallo, No se pudo insertar los registros");
            }
        } catch (Exception e) {
            logger.error("createDownSIM fallo", e);
        }
        return retorno;
    }

    public boolean createEvento(DataToken dataToken, MimeMessage mimeMessage) {
        boolean retorno = false;
        try {
            List<EventoDTO> eventoDTOS = getObjetEvenOfString(getContentMultipartMessage(mimeMessage), dataToken);
            if (eventoDTOS.isEmpty()) {
                logger.info(" createEvento, La trama no es valida ");
            } else {
                List<EventoDTO> response = objectManagerRepository.insert(dataToken, new ObjectManagerInsertRequest(eventoDTOS), false, EventoDTO.class);
                if (response == null) {
                    logger.error("createEvento fallo, No se pudo insertar los registros");
                }
                retorno = true;
            }
        } catch (Exception e) {
            logger.error(" ProsegurService  ->  createEvento Fallo", e);
        }
        return retorno;
    }

    private List<EventoDTO> getObjetEvenOfString(String bodyEmail, DataToken dataToken) {
        String[] eventParts = bodyEmail.split(";");
        List<EventoDTO> eventoDTOs = new ArrayList<>(0);
        if (eventParts.length == 8) {
            EventoDTO event = new EventoDTO(dataToken.getVersionAplicacion() + "_" + Math.round((Math.random() * (900))) + "_" + ThreadLocalRandom.current().nextInt(), eventParts);
            eventoDTOs.add(event);
        }
        return eventoDTOs;
    }

    private String getContentMultipartMessage(MimeMessage mimeMessage) {
        String result = "";
        try {
            if (mimeMessage.isMimeType("text/plain")) {
                result = mimeMessage.getContent().toString();
            } else if (mimeMessage.isMimeType("text/html")) {
                result = mimeMessage.getContent().toString();
            } else if (mimeMessage.isMimeType("multipart/*")) {
                MimeMultipart mimeMultipart = (MimeMultipart) mimeMessage.getContent();
                result = getTextFromMimeMultipart(mimeMultipart);
            }
        } catch (Exception e) {
            logger.error("  ProsegurService  ->  getContentMultipartMessage Fallo: ", e);
        }
        return result;
    }

    private String getTextFromMimeMultipart(MimeMultipart mimeMultipart) {
        StringBuilder result = new StringBuilder();
        try {
            int count = mimeMultipart.getCount();
            for (int i = 0; i < count; i++) {
                BodyPart bodyPart = mimeMultipart.getBodyPart(i);
                if (bodyPart.isMimeType("text/plain")) {
                    result.append(bodyPart.getContent());
                    break; // without break same text appears twice in my tests
                } else if (bodyPart.isMimeType("text/html")) {
                    String html = (String) bodyPart.getContent();
                    result.append(Jsoup.parse(html).text());
                } else if (bodyPart.getContent() instanceof MimeMultipart) {
                    result.append(getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent()));
                }
            }
        } catch (Exception e) {
            logger.error("  ProsegurService  ->  getTextFromMimeMultipart  Fallo: ", e);
        }
        return result.toString();
    }

    public boolean executeDownSIM(DataToken dataToken) {
        boolean retorno = false;
        try {
            String tokenEncriptApiRead = generarTokenGetObjeto(dataToken);
            List<CampoDTO> campos = new ArrayList<>(4);
            campos.add(new CampoDTO("icc"));
            campos.add(new CampoDTO("estado_simbaja.id"));
            ObjectManagerGetRequestDTO objectManagerGetRequestDTO = new ObjectManagerGetRequestDTO("simbaja", campos);
            List<FiltroDTO> filtros = new ArrayList<>(1);
            filtros.add(new FiltroDTO("id", "=", "estado_simbaja", "'" + idEstadoBajaProgramada + "'"));
            objectManagerGetRequestDTO.setFiltros(filtros);
            String response = objectManagerRepository.get(dataToken.getUrlApiRead(), tokenEncriptApiRead, objectManagerGetRequestDTO);
            if (response != null) {
                ObjectManagerGetSimBajaResponseDTO objectManagerGetSimBajaResponseDTO = objectMapper.readValue(response, ObjectManagerGetSimBajaResponseDTO.class);
                if (objectManagerGetSimBajaResponseDTO != null) {
                    List<SimBajaDownDTO> simBajaDownDTOs = objectManagerGetSimBajaResponseDTO.getData();
                    if ((simBajaDownDTOs != null) && (!simBajaDownDTOs.isEmpty())) {
                        logger.info("executeDownSIM, se van ejecutar la baja de " + simBajaDownDTOs.size() + " sims");
                        for (SimBajaDownDTO simBajaDownDTO : simBajaDownDTOs) {
                            String icc = simBajaDownDTO.getIcc();
                            SimBajaDTO simBajaDTO = new SimBajaDTO(icc);
                            SuscriptionDTO suscriptionDTO = kitePlatformRepository.getSuscription(icc);
                            if (suscriptionDTO == null) {
                                simBajaDTO.setMotivo("No se pudo encontrar una suscripción relacionada al ICC (" + icc + ")");
                            } else if (suscriptionDTO.getActivationDate() == null) {
                                simBajaDTO.setMotivo("La suscripcion del ICC (" + icc + ") no tiene valor en el campo ActivationDate");
                            } else if (((new Date().getTime() - suscriptionDTO.getActivationDate().getTime()) / MILLISECONDS_IN_MONTH) < simMinMonthFromActivated) {
                                simBajaDTO.setMotivo("El tiempo en meses desde la activación de la suscripción relacionada al ICC (" + icc + ") es menor que " + simMinMonthFromActivated);
                            } else {
                                suscriptionDTO.setLifeCycleStatus(lifeCycleDown);
                                if (kitePlatformRepository.modifySuscription(suscriptionDTO)) {
                                    retorno = true;
                                } else {
                                    simBajaDTO.setMotivo("No se realizo la modificación a la suscripción, en KitePlatform, relacionada al ICC (" + icc + ")");
                                }
                            }
                            simBajaDTO.setEstadoSimBaja(new EstadoSimBajaDTO(retorno ? idEstadoBajaConfirmado : idEstadoBajaInvalido));
                            objectManagerRepository.update(dataToken, simBajaDTO, false, false, false, false);
                        }
                        logger.info("executeDownSIM, termino ejecutar la baja de " + simBajaDownDTOs.size() + " sims");
                    }
                }
            }
        } catch (Exception e) {
            logger.error("executeDownSIM fallo", e);
        }
        return retorno;
    }

    public boolean executeInstallSIM(DataToken dataToken) {
        boolean retorno = false;
        try {
            List<FiltroDTO> filtros = new ArrayList<>(1);
            filtros.add(new FiltroDTO("enviado", "=", "instalacion", "'false'"));
            if (installSIM(filtros, dataToken)) {
                retorno = true;
            }
        } catch (Exception e) {
            logger.error("executeInstallSIM fallo", e);
        }
        return retorno;
    }
}
