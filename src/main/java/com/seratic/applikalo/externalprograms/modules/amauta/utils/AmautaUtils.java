package com.seratic.applikalo.externalprograms.modules.amauta.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.seratic.applikalo.externalprograms.modules.amauta.VOs.LeccionVO;
import com.seratic.applikalo.externalprograms.modules.integracion.entities.errors.MyErrorListException;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static com.seratic.applikalo.externalprograms.modules.integracion.Constants.WF_ESTADO_MANUAL;
import static com.seratic.applikalo.externalprograms.modules.integracion.utils.FechasUtils.formatDateConSinTimeZoneSave;

public class AmautaUtils {

    public final static JavaMailSenderImpl configurarPropiedadesEmail(String username, String password) {
        try {
            JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
            mailSender.setHost("smtp.gmail.com");
            mailSender.setPort(587);
            mailSender.setUsername(username);
            mailSender.setPassword(password);

            Properties javaMailProperties = new Properties();
            javaMailProperties.put("mail.smtp.starttls.enable", "true");
            javaMailProperties.put("mail.smtp.auth", "true");
            javaMailProperties.put("mail.transport.protocol", "smtp");
            javaMailProperties.put("mail.debug", "true");
            mailSender.setJavaMailProperties(javaMailProperties);
            return mailSender;
        } catch (Exception e) {
            throw e;
        }
    }

    public final static MimeMessageHelper armarCorreo(MimeMessage msg, String[] destinatario,
                                                      String asunto, String cuerpo,
                                                      String nombreArchivo, ByteArrayResource file) throws MessagingException {
        MimeMessageHelper helper = new MimeMessageHelper(msg, true);
        helper.setTo(destinatario);
        helper.setSubject(asunto);
        helper.setText(cuerpo, true);
        if (file != null) {
            helper.addAttachment(nombreArchivo, file);
        }
        return helper;
    }

    public static String getBodyEstudianteCursoExiste(String idEstudiante, String idCurso, String idEstudCurso) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();

            List<JsonNode> filtros = new ArrayList<>();
            JsonNode filtro1 = mapper.createObjectNode();
            ((ObjectNode) filtro1).put("campo", "id");
            ((ObjectNode) filtro1).put("condicional", "<>");
            ((ObjectNode) filtro1).put("conjuncion", "AND");
            ((ObjectNode) filtro1).put("objeto", "estudiante_curso");
            ((ObjectNode) filtro1).put("parametro", idEstudCurso);
            filtros.add(filtro1);

            JsonNode filtro2 = mapper.createObjectNode();
            ((ObjectNode) filtro2).put("campo", "activo");
            ((ObjectNode) filtro2).put("condicional", "=");
            ((ObjectNode) filtro2).put("conjuncion", "AND");
            ((ObjectNode) filtro2).put("objeto", "estudiante_curso");
            ((ObjectNode) filtro2).put("parametro", "'true'");
            filtros.add(filtro2);

            JsonNode filtro3 = mapper.createObjectNode();
            ((ObjectNode) filtro3).put("campo", "id");
            ((ObjectNode) filtro3).put("condicional", "=");
            ((ObjectNode) filtro3).put("conjuncion", "AND");
            ((ObjectNode) filtro3).put("objeto", "estudiante_curso.estudiante");
            ((ObjectNode) filtro3).put("parametro", idEstudiante);
            filtros.add(filtro3);

            JsonNode filtro4 = mapper.createObjectNode();
            ((ObjectNode) filtro4).put("campo", "id");
            ((ObjectNode) filtro4).put("condicional", "=");
            ((ObjectNode) filtro4).put("conjuncion", "AND");
            ((ObjectNode) filtro4).put("objeto", "estudiante_curso.curso");
            ((ObjectNode) filtro4).put("parametro", idCurso);
            filtros.add(filtro4);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 500000);
            ((ObjectNode) objetoPeticion).put("objeto", "estudiante_curso");

            List<JsonNode> campos = new ArrayList<>();
            JsonNode campoActivo = mapper.createObjectNode();
            ((ObjectNode) campoActivo).put("campo", "activo");
            campos.add(campoActivo);

            JsonNode campoIdEstudiante = mapper.createObjectNode();
            ((ObjectNode) campoIdEstudiante).put("campo", "estudiante.id");
            campos.add(campoIdEstudiante);

            JsonNode campoIdCurso = mapper.createObjectNode();
            ((ObjectNode) campoIdCurso).put("campo", "curso.id");
            campos.add(campoIdCurso);

            JsonNode campoIdPeriodo = mapper.createObjectNode();
            ((ObjectNode) campoIdPeriodo).put("campo", "periodo.id");
            campos.add(campoIdPeriodo);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body estudiante_curso", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static String getBodyEstudianteCursoById(String idEstudianteCurso) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();

            List<JsonNode> filtros = new ArrayList<>();
            JsonNode filtro1 = mapper.createObjectNode();
            ((ObjectNode) filtro1).put("campo", "id");
            ((ObjectNode) filtro1).put("condicional", "=");
            ((ObjectNode) filtro1).put("conjuncion", "AND");
            ((ObjectNode) filtro1).put("objeto", "estudiante_curso");
            ((ObjectNode) filtro1).put("parametro", idEstudianteCurso);
            filtros.add(filtro1);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 1);
            ((ObjectNode) objetoPeticion).put("objeto", "estudiante_curso");

            List<JsonNode> campos = new ArrayList<>();
            JsonNode campoActivo = mapper.createObjectNode();
            ((ObjectNode) campoActivo).put("campo", "activo");
            campos.add(campoActivo);

            JsonNode campoIdEstudiante = mapper.createObjectNode();
            ((ObjectNode) campoIdEstudiante).put("campo", "estudiante.id");
            campos.add(campoIdEstudiante);

            JsonNode campoIdCurso = mapper.createObjectNode();
            ((ObjectNode) campoIdCurso).put("campo", "curso.id");
            campos.add(campoIdCurso);

            JsonNode campoIdPeriodo = mapper.createObjectNode();
            ((ObjectNode) campoIdPeriodo).put("campo", "periodo.id");
            campos.add(campoIdPeriodo);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body estudiante_curso", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static String getBodyLecciones(String idCurso) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();

            List<JsonNode> filtros = new ArrayList<>();
            JsonNode filtro1 = mapper.createObjectNode();
            ((ObjectNode) filtro1).put("campo", "id");
            ((ObjectNode) filtro1).put("condicional", "=");
            ((ObjectNode) filtro1).put("objeto", "leccion.modulo.curso");
            ((ObjectNode) filtro1).put("parametro", idCurso);
            filtros.add(filtro1);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 500000);
            ((ObjectNode) objetoPeticion).put("objeto", "leccion");

            List<JsonNode> campos = new ArrayList<>();
            JsonNode campo1 = mapper.createObjectNode();
            ((ObjectNode) campo1).put("campo", "modulo.curso.id");
            campos.add(campo1);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body lecciones", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static String getBodyLeccionesExist(String idEstudiante, String idCurso) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode objetoPeticion = mapper.createObjectNode();

            List<JsonNode> filtros = new ArrayList<>();
            JsonNode filtro1 = mapper.createObjectNode();
            ((ObjectNode) filtro1).put("campo", "id");
            ((ObjectNode) filtro1).put("condicional", "=");
            ((ObjectNode) filtro1).put("conjuncion", "AND");
            ((ObjectNode) filtro1).put("objeto", "estudiante_leccion.estudiante");
            ((ObjectNode) filtro1).put("parametro", idEstudiante);
            filtros.add(filtro1);

            JsonNode filtro2 = mapper.createObjectNode();
            ((ObjectNode) filtro2).put("campo", "activo");
            ((ObjectNode) filtro2).put("condicional", "=");
            ((ObjectNode) filtro2).put("conjuncion", "AND");
            ((ObjectNode) filtro2).put("objeto", "estudiante_leccion");
            ((ObjectNode) filtro2).put("parametro", "'true'");
            filtros.add(filtro2);

            JsonNode filtro3 = mapper.createObjectNode();
            ((ObjectNode) filtro3).put("campo", "id");
            ((ObjectNode) filtro3).put("condicional", "=");
            ((ObjectNode) filtro3).put("objeto", "estudiante_leccion.leccion.modulo.curso");
            ((ObjectNode) filtro3).put("parametro", idCurso);
            filtros.add(filtro3);

            ((ObjectNode) objetoPeticion).putArray("filtros").addAll(filtros);
            ((ObjectNode) objetoPeticion).put("page", 1);
            ((ObjectNode) objetoPeticion).put("start", 0);
            ((ObjectNode) objetoPeticion).put("pagesize", 500000);
            ((ObjectNode) objetoPeticion).put("objeto", "estudiante_leccion");

            List<JsonNode> campos = new ArrayList<>();
            JsonNode campo1 = mapper.createObjectNode();
            ((ObjectNode) campo1).put("campo", "activo");
            campos.add(campo1);

            JsonNode campo2 = mapper.createObjectNode();
            ((ObjectNode) campo2).put("campo", "estudiante.id");
            campos.add(campo2);

            JsonNode campo3 = mapper.createObjectNode();
            ((ObjectNode) campo3).put("campo", "leccion.modulo.curso.id");
            campos.add(campo3);

            ((ObjectNode) objetoPeticion).putArray("campos").addAll(campos);
            return objetoPeticion.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error obteniendo body estudiante_leccion", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static final String getBodyUpadateLeccionExist(JsonNode estudLeccionExist) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode camposObj = mapper.createObjectNode();
            ((ObjectNode) camposObj).put("id", estudLeccionExist.get("id").asText());
            ((ObjectNode) camposObj).put("activo", false);

            JsonNode objUpdte = mapper.createObjectNode();
            ((ObjectNode) objUpdte).put("estudiante_leccion", camposObj);
            return objUpdte.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error getBodyUpadateLeccionesExist generando body update estudiante_leccion", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static final String getBodyActivarEstudCurso(JsonNode estudianteCurso, boolean activo) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode camposObj = mapper.createObjectNode();
            ((ObjectNode) camposObj).put("id", estudianteCurso.get("id").asText());
            ((ObjectNode) camposObj).put("activo", activo);

            JsonNode objUpdte = mapper.createObjectNode();
            ((ObjectNode) objUpdte).put("estudiante_curso", camposObj);
            return objUpdte.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error getBodyUpadateLeccionesExist generando body update estudiante_curso " + estudianteCurso.get("id").asText(), "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static final String getBodyEstadObjeto(String idObjeto, String nombreObjeto, String nombreEstado) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode camposObj = mapper.createObjectNode();
            ((ObjectNode) camposObj).put("id", idObjeto);

            JsonNode campoEstado = mapper.createObjectNode();
            ((ObjectNode) campoEstado).put("nombre", nombreEstado);
            ((ObjectNode) camposObj).put("estado_" + nombreObjeto, campoEstado);

            JsonNode objUpdte = mapper.createObjectNode();
            ((ObjectNode) objUpdte).put(nombreObjeto, camposObj);
            return objUpdte.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error getBodyEstadObjeto generando body update " + nombreObjeto + " id: " + idObjeto, "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static final String getBodyInsertEstudLecciones(String idEstudiante, JsonNode lecciones) {
        MyErrorListException errList = new MyErrorListException();
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<JsonNode> listaObjetos = new ArrayList<>();

            JsonNode objEstudiante = mapper.createObjectNode();
            ((ObjectNode) objEstudiante).put("id", idEstudiante);
            for (JsonNode leccion : lecciones) {
                JsonNode objLeccion = mapper.createObjectNode();
                ((ObjectNode) objLeccion).put("id", leccion.get("id").asText());

                JsonNode camposObj = mapper.createObjectNode();
                ((ObjectNode) camposObj).put("estudiante", objEstudiante);
                ((ObjectNode) camposObj).put("leccion", objLeccion);
                ((ObjectNode) camposObj).put("activo", true);
                ((ObjectNode) camposObj).put("procesado", false);

                JsonNode objUpdte = mapper.createObjectNode();
                ((ObjectNode) objUpdte).put("estudiante_leccion", camposObj);
                listaObjetos.add(objUpdte);
            }

            JsonNode objToSave = mapper.createObjectNode();
            ((ObjectNode) objToSave).putArray("objetos").addAll(listaObjetos);
            return objToSave.toString();
        } catch (Exception e) {
            errList.addError("10.2", "Error insertregisterpost generando body insert estudiante_leccion", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static List<LeccionVO> getLeccionesVOLst(JsonNode estudLeccionLstResp) {
        MyErrorListException errList = new MyErrorListException();
        try {
            List<LeccionVO> leccionVOList = new ArrayList<>();
            for (JsonNode estudLeccionResp : estudLeccionLstResp) {
                LeccionVO leccionVO = new LeccionVO();
                leccionVO.setIdentificador(estudLeccionResp.get("id").asText());
                leccionVOList.add(leccionVO);
            }
            return leccionVOList;
        } catch (Exception e) {
            errList.addError("2.1", "Error procesando respuestaa estudiante_leccion", "ex: " + e.getMessage());
            throw errList;
        }
    }

    public static String armarObjetoWf(String nombreObjeto, String idObjeto, String nombreNodo, boolean mantenerSinTimeZone) {
        try {
            String usernameCreacion = "OWNER";
            ObjectMapper mapper = new ObjectMapper();

            JsonNode objBaseRel = mapper.createObjectNode();
            ((ObjectNode) objBaseRel).put("id", idObjeto);

            List<JsonNode> listaObjetos = new ArrayList<>();

            JsonNode atributosObjWf = mapper.createObjectNode();
            Timestamp timestamp = new Timestamp(new Date().getTime());
            String pkFlujoTrabajo = idObjeto + "|" + usernameCreacion + "|" + timestamp.getTime();

            JsonNode estado = mapper.createObjectNode();
            ((ObjectNode) estado).put("nombre", nombreNodo);

            ((ObjectNode) atributosObjWf).put("pk_flujo_trabajo", pkFlujoTrabajo);
            ((ObjectNode) atributosObjWf).put("nodo", nombreNodo);
            ((ObjectNode) atributosObjWf).put("usuario", usernameCreacion);
            ((ObjectNode) atributosObjWf).put("fecha_hora", formatDateConSinTimeZoneSave(new Date(), mantenerSinTimeZone));
            ((ObjectNode) atributosObjWf).put("usuario", usernameCreacion);
            ((ObjectNode) atributosObjWf).put(nombreObjeto, objBaseRel);
            ((ObjectNode) atributosObjWf).put("estado_" + nombreObjeto, estado);
            ((ObjectNode) atributosObjWf).put("ejecutadoPorApi", true);
            ((ObjectNode) atributosObjWf).put("paralelo", false);
            ((ObjectNode) atributosObjWf).put("tipo_nodo", WF_ESTADO_MANUAL);

            JsonNode objWf = mapper.createObjectNode();
            ((ObjectNode) objWf).put("wf_" + nombreObjeto, atributosObjWf);
            listaObjetos.add(objWf);


            JsonNode objToSave = mapper.createObjectNode();
            ((ObjectNode) objToSave).putArray("objetos").addAll(listaObjetos);

            return objToSave.toString();
        } catch (Exception e) {
            return null;
        }
    }
}
