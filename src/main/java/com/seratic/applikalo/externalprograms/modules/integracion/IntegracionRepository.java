package com.seratic.applikalo.externalprograms.modules.integracion;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.VOs.LocationVo;
import com.seratic.applikalo.externalprograms.modules.crearactualizarobjetos.utils.UtilsObjetos;
import com.seratic.applikalo.externalprograms.modules.security.VOs.DataToken;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

@Repository
public class IntegracionRepository {

    private static final Logger logger = LoggerFactory.getLogger(IntegracionRepository.class);
    private final RestTemplate restTemplate = new RestTemplate();

    /* Método para obtener los campos de un objeto en api objetos*/
    public ResponseEntity<String> getObjetoApi(DataToken dataToken, String tokenEcript, String body) {
        try {
            String url = dataToken.getUrlApiRead() + "funcion/getObjeto";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
            headers.add("token", tokenEcript);
            HttpEntity<String> requestEntity = new HttpEntity<String>(body, headers);
            //logger.info("--------->**** PETICION: " + url + " body: " + body);
            return new RestTemplate().exchange(url, HttpMethod.POST, requestEntity, String.class);
        } catch (Exception e) {
            logger.info("--------->**** ERROR PETICION: " + dataToken.getUrlApiRead() + "funcion/getObjeto body: " + body);
            throw e;
        }
    }

    /* Método para actualizar objetos en api objetos mediante servicio insertregisterpost apigo*/
    public ResponseEntity<String> updateApi(DataToken dataToken, String body, boolean ignorarTipoFlujo) {
        try {
            String auth = dataToken.getPrefijo() + "_" + dataToken.getVersionAplicacion();
            String url = dataToken.getUrlApiWrite() + "insertregisterpost";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
            headers.add("Authorization", auth);
            headers.add("idpublicuser", dataToken.getIdPublico());
            headers.add("updateDataInsert", "true");
            if (ignorarTipoFlujo) {
                headers.add("ignorarTipoFlujo", "true");
            }
            HttpEntity<String> requestEntity = new HttpEntity<String>(body, headers);
            return new RestTemplate().exchange(url, HttpMethod.POST, requestEntity, String.class);
        } catch (Exception e) {
            logger.info("--------->**** ERROR PETICION: " + dataToken.getUrlApiRead() + "insertregisterpost body: " + body);
            throw e;
        }
    }

    /* Método para insertar objetos en api objetos*/
    public ResponseEntity<String> insertApi(DataToken dataToken, String body) {
        try {
            String auth = dataToken.getPrefijo() + "_" + dataToken.getVersionAplicacion();
            String url = dataToken.getUrlApiWrite() + "insertregisterpost";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
            headers.add("Authorization", auth);
            headers.add("idpublicuser", dataToken.getIdPublico());
            HttpEntity<String> requestEntity = new HttpEntity<String>(body, headers);
            return new RestTemplate().exchange(url, HttpMethod.POST, requestEntity, String.class);
        } catch (Exception e) {
            throw e;
        }
    }

    /* Método para actualizar objetos mediante servicio update apigo*/
    public ResponseEntity<String> updateServicioUpdateApigo(DataToken dataToken, String body, boolean ignorarTipoFlujo, boolean notificar, boolean automatization) {
        try {
            String auth = dataToken.getPrefijo() + "_" + dataToken.getVersionAplicacion();
            String url = dataToken.getUrlApiWrite() + "update";
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);            
            if (!notificar) {
                headers.add("notificar", "false");               
            }
            if (!automatization) {
                headers.add("automatization","false");               
            }
            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
            headers.add("Authorization", auth);
            headers.add("idpublicuser", dataToken.getIdPublico());
            headers.add("sendUserNullNotificationMovil", "true");
            headers.add("ignorarNotificacionSecurity", "true");
            if (ignorarTipoFlujo) {
                headers.add("ignorarTipoFlujo", "true");
            }
            HttpEntity<String> requestEntity = new HttpEntity<String>(body, headers);
            logger.info("---- Update API ----\n URL: " + url + "\n Body request: " + body);
            return new RestTemplate().exchange(url, HttpMethod.PUT, requestEntity, String.class);
        } catch (Exception e) {
            logger.error("  IntegracionRepository  ->  updateServicioUpdateApigo  Fallo: ", e);
            return null;
        }
    }

    /* Método para insertar objetos en api objetos*/
    public String getResponseEntityPOST(String url, HttpHeaders headers, String body) {
        try {
            HttpEntity<String> requestEntity = new HttpEntity<String>(body, headers);
            return new RestTemplate().exchange(url, HttpMethod.POST, requestEntity, String.class).getBody();
        } catch (Exception e) {
            throw e;
        }
    }

    public String getResponseEntityGET(String url, HttpHeaders headers) {
        try {
            HttpEntity<String> requestEntity = new HttpEntity<String>(null, headers);
            return new RestTemplate().exchange(url, HttpMethod.GET, requestEntity, String.class).getBody();
        } catch (Exception e) {
            throw e;
        }
    }

    /* Método para obtener los campos de un objeto en api objetos por la fuente de datos*/
    public List<Map<String, Object>> getObjetoApiCamposPaginacion(DataToken dataToken, int idVersionApp, String prefijo, int idFuenteDatos, String idPublic, String body) {
        try {
            List<Map<String, Object>> retorno = new ArrayList<>();
            String url = dataToken.getUrlApiRead() + "funcion/campospaginacion";
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", prefijo + "_" + idVersionApp);
            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
            HttpEntity request = new HttpEntity(body, headers);
            JsonNode respose = restTemplate.exchange(url + "/" + idFuenteDatos + "/" + idVersionApp + idPublic, HttpMethod.POST, request, JsonNode.class).getBody();
            if (respose.size() > 0) {
                retorno = new ObjectMapper().convertValue(respose.get("data"), List.class);
                return retorno;
            } else {
                return retorno;
            }
        } catch (Exception e) {
            String msg = "--------->**** ERROR PETICION: " + dataToken.getUrlApiRead() + "funcion/campospaginacion";
            logger.info(msg);
            throw e;
        }
    }

    public double getDistance(DataToken dataToken, LocationVo locationVo) {
        try {
            String body = UtilsObjetos.buildBodyDistanceRequest(locationVo);
            String url = dataToken.getUrlApiRead() + "geolocation/calculateDistance";
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Authorization", "public");
            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
            HttpEntity<String> request = new HttpEntity(body, headers);
            double respose = restTemplate.exchange(url, HttpMethod.POST, request, double.class).getBody();
            return respose;
        } catch (Exception e) {
            String msg = "--------->**** ERROR PETICION: " + dataToken.getUrlApiRead() + "funcion/campospaginacion";
            logger.info(msg);
            throw e;
        }
    }
}
