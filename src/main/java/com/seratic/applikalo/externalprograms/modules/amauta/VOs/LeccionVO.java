package com.seratic.applikalo.externalprograms.modules.amauta.VOs;

public class LeccionVO {
    String identificador;

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }
}
