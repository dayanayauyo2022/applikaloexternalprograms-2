package com.seratic.applikalo.externalprograms.modules.activosmineros.dto;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public class NotificacionReporteDTO {

    List<JsonNode> compsNotificacion;
    JsonNode camposObjeto;
    String versionApp;
    String ambiente;
    String idPublicoOwner;
    String idPublicoUsuario;
    String objetoBase;
    String idObjetoBase;
    boolean mantenerSinTimeZone;

    public NotificacionReporteDTO() {
        // default implementation ignored
    }

    public List<JsonNode> getCompsNotificacion() {
        return compsNotificacion;
    }

    public void setCompsNotificacion(List<JsonNode> compsNotificacion) {
        this.compsNotificacion = compsNotificacion;
    }

    public JsonNode getCamposObjeto() {
        return camposObjeto;
    }

    public void setCamposObjeto(JsonNode camposObjeto) {
        this.camposObjeto = camposObjeto;
    }

    public String getVersionApp() {
        return versionApp;
    }

    public void setVersionApp(String versionApp) {
        this.versionApp = versionApp;
    }

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public String getIdPublicoOwner() {
        return idPublicoOwner;
    }

    public void setIdPublicoOwner(String idPublicoOwner) {
        this.idPublicoOwner = idPublicoOwner;
    }

    public String getIdPublicoUsuario() {
        return idPublicoUsuario;
    }

    public void setIdPublicoUsuario(String idPublicoUsuario) {
        this.idPublicoUsuario = idPublicoUsuario;
    }

    public String getObjetoBase() {
        return objetoBase;
    }

    public void setObjetoBase(String objetoBase) {
        this.objetoBase = objetoBase;
    }

    public String getIdObjetoBase() {
        return idObjetoBase;
    }

    public void setIdObjetoBase(String idObjetoBase) {
        this.idObjetoBase = idObjetoBase;
    }

    public boolean isMantenerSinTimeZone() {
        return mantenerSinTimeZone;
    }

    public void setMantenerSinTimeZone(boolean mantenerSinTimeZone) {
        this.mantenerSinTimeZone = mantenerSinTimeZone;
    }

}
